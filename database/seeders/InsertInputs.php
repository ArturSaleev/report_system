<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Input;

class InsertInputs extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'label',
                'ru' => ['label' => 'Обычный Текст'],
                'en' => ['label' => 'Label']
            ],
            [
                'name' => 'checkbox',
                'ru' => ['label' => 'Галочка'],
                'en' => ['label' => 'Checkbox']
            ],
            [
                'name' => 'file',
                'ru' => ['label' => 'Загрузка файла'],
                'en' => ['label' => 'File upload']
            ],
            [
                'name' => 'hr',
                'ru' => ['label' => 'Разделитель'],
                'en' => ['label' => 'HR']
            ],
            [
                'name' => 'image',
                'ru' => ['label' => 'Рисунок'],
                'en' => ['label' => 'Image']
            ],
            [
                'name' => 'inputDate',
                'ru' => ['label' => 'Поле дата'],
                'en' => ['label' => 'Date input']
            ],
            [
                'name' => 'inputMask',
                'ru' => ['label' => 'Поле маска'],
                'en' => ['label' => 'Mask input']
            ],
            [
                'name' => 'inputNumber',
                'ru' => ['label' => 'Поле число'],
                'en' => ['label' => 'Number input']
            ],
            [
                'name' => 'inputString',
                'ru' => ['label' => 'Поле текст'],
                'en' => ['label' => 'Text input']
            ],
            [
                'name' => 'radio',
                'ru' => ['label' => 'Переключатель'],
                'en' => ['label' => 'Radio button']
            ],
            [
                'name' => 'select',
                'ru' => ['label' => 'Поле выбора'],
                'en' => ['label' => 'Select input']
            ],
            [
                'name' => 'tab',
                'ru' => ['label' => 'Таб'],
                'en' => ['label' => 'Tab']
            ],
            [
                'name' => 'textarea',
                'ru' => ['label' => 'Текстовое поле'],
                'en' => ['label' => 'Textarea']
            ]
        ];
        foreach($data as $ds) {
            Input::create($ds);
        }
    }
}
