<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ListSpecial;

class InsertHR extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => '01.01.01',
                'ru' => ['name' => 'Вещественный, комплексный и функциональный анализ'],
                'en' => ['name' => 'Real, complex and functional analysis']
            ],
            [
                'code' => '01.01.02',
                'ru' => ['name' => 'Дифференциальные уравнения, динамические системы и оптимальное управление'],
                'en' => ['name' => 'Differential equations, dynamical systems and optimal control']
            ],
            [
                'code' => '01.01.03',
                'ru' => ['name' => 'Математическая физика'],
                'en' => ['name' => 'Mathematical physics']
            ],
            [
                'code' => '01.01.04',
                'ru' => ['name' => 'Геометрия и топология'],
                'en' => ['name' => 'Geometry and topology']
            ],
            [
                'code' => '01.01.05',
                'ru' => ['name' => 'Теория вероятностей и математическая статистика'],
                'en' => ['name' => 'probability Theory and mathematical statistics']
            ],
            [
                'code' => '01.01.06',
                'ru' => ['name' => 'Математическая логика, алгебра и теория чисел.'],
                'en' => ['name' => 'Mathematical logic, algebra and number theory.']
            ],
            [
                'code' => '01.01.07',
                'ru' => ['name' => 'Вычислительная математика'],
                'en' => ['name' => 'Computational mathematics']
            ],
            [
                'code' => '01.01.09',
                'ru' => ['name' => 'Дискретная математика и математическая кибернетика'],
                'en' => ['name' => 'Discrete mathematics and mathematical Cybernetics']
            ],
            [
                'code' => '01.02.01',
                'ru' => ['name' => 'Теоретическая механика'],
                'en' => ['name' => 'Theoretical mechanics']
            ],
            [
                'code' => '01.02.04',
                'ru' => ['name' => 'Механика деформируемого твердого тела.'],
                'en' => ['name' => 'Mechanics of a deformable solid body.']
            ],
            [
                'code' => '01.02.05',
                'ru' => ['name' => 'Механика жидкости, газа и плазмы'],
                'en' => ['name' => 'mechanics of liquid, gas and plasma']
            ],
            [
                'code' => '01.02.06',
                'ru' => ['name' => 'Динамика, прочность машин, приборов и аппаратуры'],
                'en' => ['name' => 'Dynamics, strength of machines, devices and equipment']
            ],
            [
                'code' => '01.02.08',
                'ru' => ['name' => 'Биомеханика'],
                'en' => ['name' => 'Biomechanics']
            ],
            [
                'code' => '01.03.01',
                'ru' => ['name' => 'Астрометрия и небесная механика'],
                'en' => ['name' => 'Astrometry and celestial mechanics']
            ],
            [
                'code' => '01.03.02',
                'ru' => ['name' => 'Астрофизика и звездная астрономия'],
                'en' => ['name' => 'Astrophysics and stellar astronomy']
            ],
            [
                'code' => '01.03.03',
                'ru' => ['name' => 'Физика Солнца'],
                'en' => ['name' => 'Physics Of The Sun']
            ],
            [
                'code' => '01.03.04',
                'ru' => ['name' => 'Планетные исследования'],
                'en' => ['name' => 'Planetary research']
            ],
            [
                'code' => '01.04.01',
                'ru' => ['name' => 'Приборы и методы экспериментальной физики'],
                'en' => ['name' => 'Devices and methods of experimental physics']
            ],
            [
                'code' => '01.04.02',
                'ru' => ['name' => 'Теоретическая физика'],
                'en' => ['name' => 'Theoretical physics']
            ],
            [
                'code' => '01.04.03',
                'ru' => ['name' => 'Радиофизика'],
                'en' => ['name' => 'Radiophysics']
            ],
            [
                'code' => '01.04.04',
                'ru' => ['name' => 'Физическая электроника'],
                'en' => ['name' => 'Physical electronics']
            ],
            [
                'code' => '01.04.05',
                'ru' => ['name' => 'Оптика'],
                'en' => ['name' => 'Optics']
            ],
            [
                'code' => '01.04.06',
                'ru' => ['name' => 'Акустика'],
                'en' => ['name' => 'Acoustics']
            ],
            [
                'code' => '01.04.07',
                'ru' => ['name' => 'Физика конденсированного состояния'],
                'en' => ['name' => 'condensed matter Physics']
            ],
            [
                'code' => '01.04.08',
                'ru' => ['name' => 'Физика плазмы'],
                'en' => ['name' => 'plasma Physics']
            ],
            [
                'code' => '01.04.09',
                'ru' => ['name' => 'Физика низких температур'],
                'en' => ['name' => 'low temperature Physics']
            ],
            [
                'code' => '01.04.10',
                'ru' => ['name' => 'Физика полупроводников'],
                'en' => ['name' => 'Physics of semiconductors']
            ],
            [
                'code' => '01.04.11',
                'ru' => ['name' => 'Физика магнитных явлений'],
                'en' => ['name' => 'Physics of magnetic phenomena']
            ],
            [
                'code' => '01.04.13',
                'ru' => ['name' => 'Электрофизика, электрофизические установки'],
                'en' => ['name' => 'Electrophysics, electrophysical installations']
            ],
            [
                'code' => '01.04.14',
                'ru' => ['name' => 'Теплофизика и теоретическая теплотехника'],
                'en' => ['name' => 'Thermophysics and theoretical heat engineering']
            ],
            [
                'code' => '01.04.15',
                'ru' => ['name' => 'Физика и технология наноструктур, атомная и молекул'],
                'en' => ['name' => 'Physics and technology of nanostructures, atomic and molecules']
            ],
            [
                'code' => '01.04.16',
                'ru' => ['name' => 'Физика атомного ядра и элементарных частиц'],
                'en' => ['name' => 'Physics of the atomic nucleus and elementary particles']
            ],
            [
                'code' => '01.04.17',
                'ru' => ['name' => 'Химическая физика, горение и взрыв, физика экстрема'],
                'en' => ['name' => 'Chemical physics, combustion and explosion, physics of extreme art.']
            ],
            [
                'code' => '01.04.18',
                'ru' => ['name' => 'Кристаллография, физика кристаллов'],
                'en' => ['name' => 'crystallography, physics of crystals']
            ],
            [
                'code' => '01.04.20',
                'ru' => ['name' => 'Физика пучков заряженных частиц и ускорительная техника'],
                'en' => ['name' => 'physics of charged particle beams and accelerator technology']
            ],
            [
                'code' => '01.04.21',
                'ru' => ['name' => 'Лазерная физика'],
                'en' => ['name' => 'Laser physics']
            ],
            [
                'code' => '01.04.23',
                'ru' => ['name' => 'Физика высоких энергий'],
                'en' => ['name' => 'high energy Physics']
            ],
            [
                'code' => '02.00.01',
                'ru' => ['name' => 'Неорганическая химия'],
                'en' => ['name' => 'Inorganic chemistry']
            ],
            [
                'code' => '02.00.02',
                'ru' => ['name' => 'Аналитическая химия'],
                'en' => ['name' => 'Analytical chemistry']
            ],
            [
                'code' => '02.00.03',
                'ru' => ['name' => 'Органическая химия'],
                'en' => ['name' => 'Organic chemistry']
            ],
            [
                'code' => '02.00.04',
                'ru' => ['name' => 'Физическая химия'],
                'en' => ['name' => 'Physical chemistry']
            ],
            [
                'code' => '02.00.05',
                'ru' => ['name' => 'Электрохимия'],
                'en' => ['name' => 'electrochemistry']
            ],
            [
                'code' => '02.00.06',
                'ru' => ['name' => 'Высокомолекулярные соединения'],
                'en' => ['name' => 'high Molecular weight compounds']
            ],
            [
                'code' => '02.00.08',
                'ru' => ['name' => 'Химия элементоорганических соединений'],
                'en' => ['name' => 'Chemistry of hetero-organic compounds']
            ],
            [
                'code' => '02.00.09',
                'ru' => ['name' => 'Химия высоких энергий'],
                'en' => ['name' => 'high energy Chemistry']
            ],
            [
                'code' => '02.00.10',
                'ru' => ['name' => 'Биоорганическая химия'],
                'en' => ['name' => 'Bioorganic chemistry']
            ],
            [
                'code' => '02.00.11',
                'ru' => ['name' => 'Коллоидная химия'],
                'en' => ['name' => 'Colloidal chemistry']
            ],
            [
                'code' => '02.00.12',
                'ru' => ['name' => 'Бионеорганическая химия'],
                'en' => ['name' => 'bio-Organic chemistry']
            ],
            [
                'code' => '02.00.13',
                'ru' => ['name' => 'Нефтехимия'],
                'en' => ['name' => 'Petrochemicals']
            ],
            [
                'code' => '02.00.14',
                'ru' => ['name' => 'Радиохимия'],
                'en' => ['name' => 'radiochemistry']
            ],
            [
                'code' => '02.00.15',
                'ru' => ['name' => 'Кинетика и катализ'],
                'en' => ['name' => 'Kinetics and catalysis']
            ],
            [
                'code' => '02.00.16',
                'ru' => ['name' => 'Медицинская химия'],
                'en' => ['name' => 'Medical chemistry']
            ],
            [
                'code' => '02.00.17',
                'ru' => ['name' => 'Математическая и квантовая химия'],
                'en' => ['name' => 'Mathematical and quantum chemistry']
            ],
            [
                'code' => '02.00.21',
                'ru' => ['name' => 'Химия твердого тела'],
                'en' => ['name' => 'solid state Chemistry']
            ],
            [
                'code' => '03.01.01',
                'ru' => ['name' => 'Радиобиология'],
                'en' => ['name' => 'Radiobiology']
            ],
            [
                'code' => '03.01.02',
                'ru' => ['name' => 'Биофизика'],
                'en' => ['name' => 'Biophysics']
            ],
            [
                'code' => '03.01.03',
                'ru' => ['name' => 'Молекулярная биология'],
                'en' => ['name' => 'Molecular biology']
            ],
            [
                'code' => '03.01.04',
                'ru' => ['name' => 'Биохимия'],
                'en' => ['name' => 'Biochemistry']
            ],
            [
                'code' => '03.01.05',
                'ru' => ['name' => 'Физиология и биохимия растений'],
                'en' => ['name' => 'plant Physiology and biochemistry']
            ],
            [
                'code' => '03.01.06',
                'ru' => ['name' => 'Биотехнология (в том числе бионанотехнологии)'],
                'en' => ['name' => 'Biotechnology (including bionanotechnologies)']
            ],
            [
                'code' => '03.01.07',
                'ru' => ['name' => 'Молекулярная генетика'],
                'en' => ['name' => 'Molecular genetics']
            ],
            [
                'code' => '03.01.08',
                'ru' => ['name' => 'Биоинженерия'],
                'en' => ['name' => 'bioengineering']
            ],
            [
                'code' => '03.01.09',
                'ru' => ['name' => 'Математическая биология, биоинформатика'],
                'en' => ['name' => 'Mathematical biology, bioinformatics']
            ],
            [
                'code' => '03.02.01',
                'ru' => ['name' => 'Ботаника'],
                'en' => ['name' => 'Botany']
            ],
            [
                'code' => '03.02.02',
                'ru' => ['name' => 'Вирусология'],
                'en' => ['name' => 'Virology']
            ],
            [
                'code' => '03.02.03',
                'ru' => ['name' => 'Микробиология'],
                'en' => ['name' => 'Microbiology']
            ],
            [
                'code' => '03.02.04',
                'ru' => ['name' => 'Зоология'],
                'en' => ['name' => 'Zoology']
            ],
            [
                'code' => '03.02.05',
                'ru' => ['name' => 'Энтомология'],
                'en' => ['name' => 'Entomology']
            ],
            [
                'code' => '03.02.06',
                'ru' => ['name' => 'Ихтиология'],
                'en' => ['name' => 'Ichthyology']
            ],
            [
                'code' => '03.02.07',
                'ru' => ['name' => 'Генетика'],
                'en' => ['name' => 'Genetics']
            ],
            [
                'code' => '03.02.08',
                'ru' => ['name' => 'Экология (по отраслям)'],
                'en' => ['name' => 'Ecology (by industry)']
            ],
            [
                'code' => '03.02.09',
                'ru' => ['name' => 'Биогеохимия'],
                'en' => ['name' => 'biogeochemistry']
            ],
            [
                'code' => '03.02.10',
                'ru' => ['name' => 'Гидробиология'],
                'en' => ['name' => 'Hydrobiology']
            ],
            [
                'code' => '03.02.11',
                'ru' => ['name' => 'Паразитология'],
                'en' => ['name' => 'Parasitology']
            ],
            [
                'code' => '03.02.12',
                'ru' => ['name' => 'Микология'],
                'en' => ['name' => 'Mycology']
            ],
            [
                'code' => '03.02.13',
                'ru' => ['name' => 'Почвоведение'],
                'en' => ['name' => 'soil science']
            ],
            [
                'code' => '03.02.14',
                'ru' => ['name' => 'Биологические ресурсы'],
                'en' => ['name' => 'Biological resources']
            ],
            [
                'code' => '03.03.01',
                'ru' => ['name' => 'Физиология'],
                'en' => ['name' => 'Physiology']
            ],
            [
                'code' => '03.03.02',
                'ru' => ['name' => 'Антропология'],
                'en' => ['name' => 'Anthropology']
            ],
            [
                'code' => '03.03.03',
                'ru' => ['name' => 'Иммунология'],
                'en' => ['name' => 'Immunology']
            ],
            [
                'code' => '03.03.04',
                'ru' => ['name' => 'Клеточная биология, цитология, гистология'],
                'en' => ['name' => 'Cell biology, Cytology, histology']
            ],
            [
                'code' => '03.03.05',
                'ru' => ['name' => 'Биология развития, эмбриология'],
                'en' => ['name' => 'developmental Biology, embryology']
            ],
            [
                'code' => '03.03.06',
                'ru' => ['name' => 'Нейробиология'],
                'en' => ['name' => 'Neurobiology']
            ],
            [
                'code' => '05.01.01',
                'ru' => ['name' => 'Инженерная геометрия и компьютерная графика'],
                'en' => ['name' => 'Engineering geometry and computer graphics']
            ],
            [
                'code' => '05.02.02',
                'ru' => ['name' => 'Машиноведение, системы приводов и детали машин'],
                'en' => ['name' => 'mechanical engineering, drive systems and machine parts']
            ],
            [
                'code' => '05.02.04',
                'ru' => ['name' => 'Трение и износ в машинах'],
                'en' => ['name' => 'Friction and wear in machines']
            ],
            [
                'code' => '05.02.05',
                'ru' => ['name' => 'Роботы, мехатроника и робототехнические системы'],
                'en' => ['name' => 'Robots, mechatronics and robotic systems']
            ],
            [
                'code' => '05.02.07',
                'ru' => ['name' => 'Технология и оборудование механической и физико-технической обработки'],
                'en' => ['name' => 'Technology and equipment of mechanical and physical-technical processing']
            ],
            [
                'code' => '05.02.08',
                'ru' => ['name' => 'Технология машиностроения'],
                'en' => ['name' => 'Technology of mechanical engineering']
            ],
            [
                'code' => '05.02.09',
                'ru' => ['name' => 'Технологии и машины обработки давлением'],
                'en' => ['name' => 'technologies and machines of pressure treatment']
            ],
            [
                'code' => '05.02.10',
                'ru' => ['name' => 'Сварка, родственные процессы и технологии'],
                'en' => ['name' => 'welding, related processes and technologies']
            ],
            [
                'code' => '05.02.11',
                'ru' => ['name' => 'Методы контроля и диагностика в машиностроении'],
                'en' => ['name' => 'methods of control and diagnostics in mechanical engineering']
            ],
            [
                'code' => '05.02.13',
                'ru' => ['name' => 'Машины, агрегаты и процессы (по отраслям)'],
                'en' => ['name' => 'Machines, aggregates and processes (by industry)']
            ],
            [
                'code' => '05.02.18',
                'ru' => ['name' => 'Теория механизмов и машин'],
                'en' => ['name' => 'Theory of mechanisms and machines']
            ],
            [
                'code' => '05.02.22',
                'ru' => ['name' => 'Организация производства (по отраслям)'],
                'en' => ['name' => 'organization of production (by industry)']
            ],
            [
                'code' => '05.02.23',
                'ru' => ['name' => 'Стандартизация и управление качеством продукции'],
                'en' => ['name' => 'Standardization and quality management of products']
            ],
            [
                'code' => '05.04.02',
                'ru' => ['name' => 'Тепловые двигатели'],
                'en' => ['name' => 'Heat engines']
            ],
            [
                'code' => '05.04.03',
                'ru' => ['name' => 'Машины и аппараты, процессы холодильной и криогенной техники, систем кондиционирования и жизнеобеспечения'],
                'en' => ['name' => 'Machines and apparatuses, processes of refrigeration and cryogenic equipment, air conditioning and life support systems']
            ],
            [
                'code' => '05.04.06',
                'ru' => ['name' => 'Вакуумная, компрессорная техника и пневмосистемы'],
                'en' => ['name' => 'Vacuum, compressor equipment and pneumatic systems']
            ],
            [
                'code' => '05.04.11',
                'ru' => ['name' => 'Атомное реакторостроение, машины, агрегаты и технология материалов атомной промышленности'],
                'en' => ['name' => 'Nuclear reactor engineering, machines, aggregates and materials technology of the nuclear industry']
            ],
            [
                'code' => '05.04.12',
                'ru' => ['name' => 'Турбомашины и комбинированные турбоустановки'],
                'en' => ['name' => 'Turbomachines and combined turbine']
            ],
            [
                'code' => '05.04.13',
                'ru' => ['name' => 'Гидравлические машины, гидропневмоагрегаты'],
                'en' => ['name' => 'Hydraulic machines, hydro-pneumatic units']
            ],
            [
                'code' => '05.05.03',
                'ru' => ['name' => 'Колесные и гусеничные машины'],
                'en' => ['name' => 'Wheeled and tracked vehicles']
            ],
            [
                'code' => '05.05.04',
                'ru' => ['name' => 'Дорожные, строительные и подъемно-транспортные машины'],
                'en' => ['name' => 'Road, construction and lifting vehicles']
            ],
            [
                'code' => '05.05.06',
                'ru' => ['name' => 'Горные машины'],
                'en' => ['name' => 'Mining machines']
            ],
            [
                'code' => '05.07.01',
                'ru' => ['name' => 'Аэродинамика и процессы теплообмена летательных аппаратов'],
                'en' => ['name' => 'Aerodynamics and heat transfer processes of aircraft']
            ],
            [
                'code' => '05.07.02',
                'ru' => ['name' => 'Проектирование, конструкция и производство летательных аппаратов'],
                'en' => ['name' => 'Design, construction and production of aircraft']
            ],
            [
                'code' => '05.07.03',
                'ru' => ['name' => 'Прочность и тепловые режимы летательных аппаратов'],
                'en' => ['name' => 'Strength and thermal conditions of aircraft']
            ],
            [
                'code' => '05.07.05',
                'ru' => ['name' => 'Тепловые, электроракетные двигатели и энергетические установки летательных аппаратов'],
                'en' => ['name' => 'Thermal, electric rocket engines and power plants of aircraft']
            ],
            [
                'code' => '05.07.06',
                'ru' => ['name' => 'Наземные комплексы, стартовое оборудование, эксплуатация летательных аппаратов'],
                'en' => ['name' => 'Ground complexes, launch equipment, operation of aircraft']
            ],
            [
                'code' => '05.07.07',
                'ru' => ['name' => 'Контроль и испытание летательных аппаратов и их систем'],
                'en' => ['name' => 'Control and testing of aircraft and their systems']
            ],
            [
                'code' => '05.07.09',
                'ru' => ['name' => 'Динамика, баллистика, управление движением летательных аппаратов'],
                'en' => ['name' => 'Dynamics, ballistics, motion control of aircraft']
            ],
            [
                'code' => '05.07.10',
                'ru' => ['name' => 'Инновационные технологии в аэрокосмической деятельности'],
                'en' => ['name' => 'Innovative technologies in aerospace activities']
            ],
            [
                'code' => '05.08.01',
                'ru' => ['name' => 'Теория корабля и строительная механика.'],
                'en' => ['name' => 'Theory of the ship and the building mechanics.']
            ],
            [
                'code' => '05.08.03',
                'ru' => ['name' => 'Проектирование и конструкции судов.'],
                'en' => ['name' => 'design and construction of vessels.']
            ],
            [
                'code' => '05.08.04',
                'ru' => ['name' => 'Технология судостроения, судоремонта и организация судостроительного производства'],
                'en' => ['name' => 'shipbuilding Technology, ship repair and organization of shipbuilding production']
            ],
            [
                'code' => '05.08.05',
                'ru' => ['name' => 'Судовые энергетические установки и их элементы (главные и вспомогательные)'],
                'en' => ['name' => 'Marine power plants and their components (main and auxiliary)']
            ],
            [
                'code' => '05.08.06',
                'ru' => ['name' => 'Физические поля корабля, океана, атмосферы и их взаимодействие'],
                'en' => ['name' => 'Physical fields of the ship, ocean, atmosphere and their interaction']
            ],
            [
                'code' => '05.09.01',
                'ru' => ['name' => 'Электромеханика и электрические аппараты'],
                'en' => ['name' => 'Electromechanics and electrical devices']
            ],
            [
                'code' => '05.09.02',
                'ru' => ['name' => 'Электротехнические материалы и изделия'],
                'en' => ['name' => 'Electrical materials and products']
            ],
            [
                'code' => '05.09.03',
                'ru' => ['name' => 'Электротехнические комплексы и системы'],
                'en' => ['name' => 'Electrical complexes and systems']
            ],
            [
                'code' => '05.09.05',
                'ru' => ['name' => 'Теоретическая электротехника.'],
                'en' => ['name' => 'Theoretical electrical engineering.']
            ],
            [
                'code' => '05.09.07',
                'ru' => ['name' => 'Светотехника'],
                'en' => ['name' => 'lighting Engineering']
            ],
            [
                'code' => '05.09.10',
                'ru' => ['name' => 'Электротехнология'],
                'en' => ['name' => 'Electrotechnology']
            ],
            [
                'code' => '05.09.12',
                'ru' => ['name' => 'Силовая электроника'],
                'en' => ['name' => 'Power electronics']
            ],
            [
                'code' => '05.11.01',
                'ru' => ['name' => 'Приборы и методы измерения (по видам измерений)'],
                'en' => ['name' => 'measuring Instruments and methods (by type of measurement)']
            ],
            [
                'code' => '05.11.03',
                'ru' => ['name' => 'Приборы навигации'],
                'en' => ['name' => 'navigation Devices']
            ],
            [
                'code' => '05.11.06',
                'ru' => ['name' => 'Акустические приборы и системы'],
                'en' => ['name' => 'Acoustic devices and systems']
            ],
            [
                'code' => '05.11.07',
                'ru' => ['name' => 'Оптические и оптико-электронные приборы и комплексы.'],
                'en' => ['name' => 'Optical and optoelectronic devices and complexes.']
            ],
            [
                'code' => '05.11.08',
                'ru' => ['name' => 'Радиоизмерительные приборы'],
                'en' => ['name' => 'radio Measuring devices']
            ],
            [
                'code' => '05.11.10',
                'ru' => ['name' => 'Приборы и методы для измерения ионизирующих излучений и рентгеновские приборы'],
                'en' => ['name' => 'Devices and methods for measuring ionizing radiation and x-ray devices']
            ],
            [
                'code' => '05.11.13',
                'ru' => ['name' => 'Приборы и методы контроля природной среды, веществ,материалов и изделий'],
                'en' => ['name' => 'Devices and methods for monitoring the natural environment, substances, materials and products']
            ],
            [
                'code' => '05.11.14',
                'ru' => ['name' => 'Технология приборостроения'],
                'en' => ['name' => 'instrument engineering Technology']
            ],
            [
                'code' => '05.11.15',
                'ru' => ['name' => 'Метрология и метрологическое обеспечение'],
                'en' => ['name' => 'Metrology and metrological support']
            ],
            [
                'code' => '05.11.16',
                'ru' => ['name' => 'Информационно-измерительные и управляющие системы (по отраслям)'],
                'en' => ['name' => 'Information-measuring and control systems (by industry)']
            ],
            [
                'code' => '05.11.17',
                'ru' => ['name' => 'Приборы, системы и изделия медицинского назначения'],
                'en' => ['name' => 'Medical devices, systems and products']
            ],
            [
                'code' => '05.11.18',
                'ru' => ['name' => 'Приборы и методы преобразования изображений и звука'],
                'en' => ['name' => 'Devices and methods of transformation of images and sound']
            ],
            [
                'code' => '05.12.04',
                'ru' => ['name' => 'Радиотехника, в том числе системы и устройства телевидения'],
                'en' => ['name' => 'radio engineering, including television systems and devices']
            ],
            [
                'code' => '05.12.07',
                'ru' => ['name' => 'Антенны, СВЧ устройства и их технологии'],
                'en' => ['name' => 'Antennas, microwave devices and their technologies']
            ],
            [
                'code' => '05.12.13',
                'ru' => ['name' => 'Системы, сети и устройства телекоммуникаций'],
                'en' => ['name' => 'telecommunication Systems, networks and devices']
            ],
            [
                'code' => '05.12.14',
                'ru' => ['name' => 'Радиолокация и радионавигация.'],
                'en' => ['name' => 'radar And radio navigation.']
            ],
            [
                'code' => '05.13.01',
                'ru' => ['name' => 'Системный анализ, управление и обработка информации'],
                'en' => ['name' => 'System analysis, management and information processing']
            ],
            [
                'code' => '05.13.05',
                'ru' => ['name' => 'Элементы и устройства вычислительной техники и систем управления'],
                'en' => ['name' => 'Elements and devices of computer equipment and control systems']
            ],
            [
                'code' => '05.13.06',
                'ru' => ['name' => 'Автоматизация и управление технологическими процесс'],
                'en' => ['name' => 'automation and control of technological process']
            ],
            [
                'code' => '05.13.10',
                'ru' => ['name' => 'Управление в социальных и экономических системах'],
                'en' => ['name' => 'Management in social and economic systems']
            ],
            [
                'code' => '05.13.11',
                'ru' => ['name' => 'Математическое и программное обеспечение вычислительных машин ,комплексов компьютерных сетей'],
                'en' => ['name' => 'Mathematical and software support for computers, computer network complexes']
            ],
            [
                'code' => '05.13.12',
                'ru' => ['name' => 'Системы автоматизации проектирования (по отраслям)'],
                'en' => ['name' => 'Systems of automation of designing (on branches)']
            ],
            [
                'code' => '05.13.15',
                'ru' => ['name' => 'Вычислительные машины, комплексы и компьютерные сети'],
                'en' => ['name' => 'Computers, complexes and computer networks']
            ],
            [
                'code' => '05.13.17',
                'ru' => ['name' => 'Теоретические основы информатики.'],
                'en' => ['name' => 'Theoretical foundations of computer science.']
            ],
            [
                'code' => '05.13.18',
                'ru' => ['name' => 'Математическое моделирование, численные методы и комплексы программ'],
                'en' => ['name' => 'Mathematical modeling, numerical methods and software packages']
            ],
            [
                'code' => '05.13.19',
                'ru' => ['name' => 'Методы и системы защиты информации, информационная'],
                'en' => ['name' => 'Methods and systems of information protection, information security']
            ],
            [
                'code' => '05.13.20',
                'ru' => ['name' => 'Квантовые методы обработки информации'],
                'en' => ['name' => 'Quantum information processing methods']
            ],
            [
                'code' => '05.14.01',
                'ru' => ['name' => 'Энергетические системы и комплексы'],
                'en' => ['name' => 'Energy systems and complexes']
            ],
            [
                'code' => '05.14.02',
                'ru' => ['name' => 'Электростанции и электроэнергетические системы'],
                'en' => ['name' => 'power Plants and electric power systems']
            ],
            [
                'code' => '05.14.03',
                'ru' => ['name' => 'Ядерные энергетические установки, включая проектирование'],
                'en' => ['name' => 'Nuclear power plants, including design']
            ],
            [
                'code' => '05.14.04',
                'ru' => ['name' => 'Промышленная теплоэнергетика'],
                'en' => ['name' => 'Industrial heat power engineering']
            ],
            [
                'code' => '05.14.08',
                'ru' => ['name' => 'Энергоустановки на основе возобновляемых видов энергии'],
                'en' => ['name' => 'power Plants based on renewable energy']
            ],
            [
                'code' => '05.14.12',
                'ru' => ['name' => 'Техника высоких напряжений'],
                'en' => ['name' => 'high voltage Engineering']
            ],
            [
                'code' => '05.14.14',
                'ru' => ['name' => 'Тепловые электрические станции, их энергетические системы и агрегаты'],
                'en' => ['name' => 'Thermal power plants, their power systems and aggregates']
            ],
            [
                'code' => '05.16.01',
                'ru' => ['name' => 'Металловедение и термическая обработка металлов и сплавов'],
                'en' => ['name' => 'metal Science and heat treatment of metals and alloys']
            ],
            [
                'code' => '05.16.02',
                'ru' => ['name' => 'Металлургия черных, цветных и редких металлов.'],
                'en' => ['name' => 'metallurgy of ferrous, non-ferrous and rare metals.']
            ],
            [
                'code' => '05.16.04',
                'ru' => ['name' => 'Литейное производство'],
                'en' => ['name' => 'Foundry production']
            ],
            [
                'code' => '05.16.05',
                'ru' => ['name' => 'Обработка металлов давлением'],
                'en' => ['name' => 'pressure Treatment of metals']
            ],
            [
                'code' => '05.16.06',
                'ru' => ['name' => 'Порошковая металлургия и композиционные материалы'],
                'en' => ['name' => 'Powder metallurgy and composite materials']
            ],
            [
                'code' => '05.16.07',
                'ru' => ['name' => 'Металлургия техногенных и вторичных ресурсов'],
                'en' => ['name' => 'metallurgy of man-made and secondary resources']
            ],
            [
                'code' => '05.16.08',
                'ru' => ['name' => 'Нанотехнологии и наноматериалы (по отраслям)'],
                'en' => ['name' => 'Nanotechnologies and nanomaterials (by industry)']
            ],
            [
                'code' => '05.16.09',
                'ru' => ['name' => 'Материаловедение (по отраслям)'],
                'en' => ['name' => 'materials Science (by industry)']
            ],
            [
                'code' => '05.17.01',
                'ru' => ['name' => 'Технология неорганических веществ'],
                'en' => ['name' => 'Technology of inorganic substances']
            ],
            [
                'code' => '05.17.02',
                'ru' => ['name' => 'Технология редких, рассеянных и радиоактивных элементов'],
                'en' => ['name' => 'Technology of rare, scattered and radioactive elements']
            ],
            [
                'code' => '05.17.03',
                'ru' => ['name' => 'Технология электрохимических процессов и защита от коррозии'],
                'en' => ['name' => 'Technology of electrochemical processes and corrosion protection']
            ],
            [
                'code' => '05.17.04',
                'ru' => ['name' => 'Технология органических веществ'],
                'en' => ['name' => 'Technology of organic substances']
            ],
            [
                'code' => '05.17.06',
                'ru' => ['name' => 'Технология и переработка полимеров и композитов'],
                'en' => ['name' => 'Technology and processing of polymers and composites']
            ],
            [
                'code' => '05.17.07',
                'ru' => ['name' => 'Химическая технология топлива и высокоэнергетически'],
                'en' => ['name' => 'Chemical technology of fuel and high energy']
            ],
            [
                'code' => '05.17.08',
                'ru' => ['name' => 'Процессы и аппараты химических технологий'],
                'en' => ['name' => 'Processes and devices of chemical technologies']
            ],
            [
                'code' => '05.17.11',
                'ru' => ['name' => 'Технология силикатных и тугоплавких неметаллических материалов'],
                'en' => ['name' => 'Technology of silicate and refractory nonmetallic materials']
            ],
            [
                'code' => '05.17.18',
                'ru' => ['name' => 'Мембраны и мембранная технология'],
                'en' => ['name' => 'Membranes and membrane technology']
            ],
            [
                'code' => '05.18.01',
                'ru' => ['name' => 'Технология обработки, хранения и переработки злаков'],
                'en' => ['name' => 'Technology of processing, storage and processing of cereals']
            ],
            [
                'code' => '05.18.04',
                'ru' => ['name' => 'Технология мясных, молочных и рыбных продуктов и холодильных производств'],
                'en' => ['name' => 'Technology of meat, dairy and fish products and refrigeration industries']
            ],
            [
                'code' => '05.18.05',
                'ru' => ['name' => 'Технология сахара и сахаристых продуктов, чая, табака и субтропических культур'],
                'en' => ['name' => 'Technology of sugar and sugary products, tea, tobacco and subtropical crops']
            ],
            [
                'code' => '05.18.06',
                'ru' => ['name' => 'Технология жиров, эфирных масел и парфюмерно-косметических продуктов'],
                'en' => ['name' => 'Technology of fats, essential oils and perfumery and cosmetic products']
            ],
            [
                'code' => '05.18.07',
                'ru' => ['name' => 'Биотехнология пищевых продуктов и биологически активных веществ'],
                'en' => ['name' => 'Biotechnology of food products and biologically active substances']
            ],
            [
                'code' => '05.18.12',
                'ru' => ['name' => 'Процессы и аппараты пищевых производств'],
                'en' => ['name' => 'Processes and devices of food production']
            ],
            [
                'code' => '05.18.15',
                'ru' => ['name' => 'Технология и товароведение продуктов функционального и специализированного назначения и общественного питания'],
                'en' => ['name' => 'Technology and commodity science of functional and specialized food products and public catering']
            ],
            [
                'code' => '05.18.17',
                'ru' => ['name' => 'Промышленное рыболовство'],
                'en' => ['name' => 'Commercial fishing']
            ],
            [
                'code' => '05.19.01',
                'ru' => ['name' => 'Материаловедение производств текстильной и легкой промышленности'],
                'en' => ['name' => 'materials Science of textile and light industry industries']
            ],
            [
                'code' => '05.19.02',
                'ru' => ['name' => 'Технология и первичная обработка текстильных материалов'],
                'en' => ['name' => 'Technology and primary processing of textile materials']
            ],
            [
                'code' => '05.19.04',
                'ru' => ['name' => 'Технология швейных изделий'],
                'en' => ['name' => 'Technology of garments']
            ],
            [
                'code' => '05.19.05',
                'ru' => ['name' => 'Технология кожи, меха, обувных и кожевенно-галантер'],
                'en' => ['name' => 'Technology of leather, fur, footwear and leather-galanter']
            ],
            [
                'code' => '05.20.01',
                'ru' => ['name' => 'Технологии и средства механизации сельского хозяйст'],
                'en' => ['name' => 'Technologies and means of agricultural mechanization']
            ],
            [
                'code' => '05.20.02',
                'ru' => ['name' => 'Электротехнологии и электрооборудование в сельском'],
                'en' => ['name' => 'electrical Technologies and electrical equipment in agriculture']
            ],
            [
                'code' => '05.20.03',
                'ru' => ['name' => 'Технологии и средства технического обслуживания в сельском хозяйстве'],
                'en' => ['name' => 'Technologies and means of maintenance in agriculture']
            ],
            [
                'code' => '05.21.01',
                'ru' => ['name' => 'Технология и машины лесозаготовок и лесного хозяйства'],
                'en' => ['name' => 'Technology and machine harvesting and forestry management']
            ],
            [
                'code' => '05.21.03',
                'ru' => ['name' => 'Технология и оборудование химической переработки биомассы дерева; химия древесины'],
                'en' => ['name' => 'Technology and equipment for chemical processing of wood biomass; wood chemistry']
            ],
            [
                'code' => '05.21.05',
                'ru' => ['name' => 'Древесиноведение, технология и оборудование деревопереработки'],
                'en' => ['name' => 'wood Science, technology and equipment of wood processing']
            ],
            [
                'code' => '05.22.01',
                'ru' => ['name' => 'Транспортные и транспортно-технологические системы'],
                'en' => ['name' => 'Transport and transport-technological systems']
            ],
            [
                'code' => '05.22.06',
                'ru' => ['name' => 'Железнодорожный путь, изыскание и проектирование железных дорог'],
                'en' => ['name' => 'Railway track, railway survey and design']
            ],
            [
                'code' => '05.22.07',
                'ru' => ['name' => 'Подвижной состав железных дорог, тяга поездов и электрификация'],
                'en' => ['name' => 'Rolling stock of Railways, traction of trains and electrification']
            ],
            [
                'code' => '05.22.08',
                'ru' => ['name' => 'Управление процессами перевозок'],
                'en' => ['name' => 'Management of processes of transportations']
            ],
            [
                'code' => '05.22.10',
                'ru' => ['name' => 'Эксплуатация автомобильного транспорта'],
                'en' => ['name' => 'Exploitation of automobile transport']
            ],
            [
                'code' => '05.22.13',
                'ru' => ['name' => 'Навигация и управление воздушным движением'],
                'en' => ['name' => 'air Navigation and air traffic control']
            ],
            [
                'code' => '05.22.14',
                'ru' => ['name' => 'Эксплуатация воздушного транспорта'],
                'en' => ['name' => 'Operation of air transport']
            ],
            [
                'code' => '05.22.17',
                'ru' => ['name' => 'Водные пути сообщения и гидрография'],
                'en' => ['name' => 'waterways of the message and hydrography']
            ],
            [
                'code' => '05.22.19',
                'ru' => ['name' => 'Эксплуатация водного транспорта, судовождение'],
                'en' => ['name' => 'Exploitation of water transport, navigation']
            ],
            [
                'code' => '05.23.01',
                'ru' => ['name' => 'Строительные конструкции, здания и сооружения'],
                'en' => ['name' => 'Building structures, buildings and structures']
            ],
            [
                'code' => '05.23.02',
                'ru' => ['name' => 'Основания и фундаменты, подземные сооружения'],
                'en' => ['name' => 'Foundations and foundations, underground structures']
            ],
            [
                'code' => '05.23.03',
                'ru' => ['name' => 'Теплоснабжение, вентиляция, кондиционирование воздуха'],
                'en' => ['name' => 'heat Supply, ventilation, air conditioning']
            ],
            [
                'code' => '05.23.04',
                'ru' => ['name' => 'Водоснабжение, канализация, строительные системы охраны водных ресурсов'],
                'en' => ['name' => 'water Supply, Sewerage, construction systems of water resources protection']
            ],
            [
                'code' => '05.23.05',
                'ru' => ['name' => 'Строительные материалы и изделия'],
                'en' => ['name' => 'Building materials and products']
            ],
            [
                'code' => '05.23.07',
                'ru' => ['name' => 'Гидротехническое строительство'],
                'en' => ['name' => 'Hydraulic engineering construction']
            ],
            [
                'code' => '05.23.08',
                'ru' => ['name' => 'Технология и организация строительства'],
                'en' => ['name' => 'Technology and organization of construction']
            ],
            [
                'code' => '05.23.11',
                'ru' => ['name' => 'Проектирование и строительство дорог, метрополитено'],
                'en' => ['name' => 'road Design and construction, metro']
            ],
            [
                'code' => '05.23.16',
                'ru' => ['name' => 'Гидравлика и инженерная гидрология'],
                'en' => ['name' => 'Hydraulics and engineering hydrology']
            ],
            [
                'code' => '05.23.17',
                'ru' => ['name' => 'Строительная механика'],
                'en' => ['name' => 'structural mechanics']
            ],
            [
                'code' => '05.23.19',
                'ru' => ['name' => 'Экологическая безопасность строительства и городского хозяйства'],
                'en' => ['name' => 'Environmental safety of construction and urban economy']
            ],
            [
                'code' => '05.23.20',
                'ru' => ['name' => 'Теория и история архитектуры, реставрация и реконструкция историко-архитектурного наследия'],
                'en' => ['name' => 'Theory and history of architecture, restoration and reconstruction of historical and architectural heritage']
            ],
            [
                'code' => '05.23.21',
                'ru' => ['name' => 'Архитектура зданий и сооружений. Творческие концепции архитектурной деятельности'],
                'en' => ['name' => 'Architecture of buildings and structures Creative concepts of architectural activity']
            ],
            [
                'code' => '05.23.22',
                'ru' => ['name' => 'Градостроительство, планировка сельскохозяйственных населенных пунктов'],
                'en' => ['name' => 'urban planning, planning of agricultural settlements']
            ],
            [
                'code' => '05.25.02',
                'ru' => ['name' => 'Документалистика, документоведение, архивоведение'],
                'en' => ['name' => 'documentary Studies, documentation studies, archival studies']
            ],
            [
                'code' => '05.25.03',
                'ru' => ['name' => 'Библиотековедение, библиографоведение и книговедени'],
                'en' => ['name' => 'library science, bibliography and book studies']
            ],
            [
                'code' => '05.25.05',
                'ru' => ['name' => 'Информационные системы и процессы'],
                'en' => ['name' => 'Information systems and processes']
            ],
            [
                'code' => '05.26.01',
                'ru' => ['name' => 'Охрана труда (по отраслям)'],
                'en' => ['name' => 'occupational health and Safety (by industry)']
            ],
            [
                'code' => '05.26.02',
                'ru' => ['name' => 'Безопасность в чрезвычайных ситуациях (по отраслям)'],
                'en' => ['name' => 'Security in emergency situations (by industry)']
            ],
            [
                'code' => '05.26.03',
                'ru' => ['name' => 'Пожарная и промышленная безопасность (по отраслям)'],
                'en' => ['name' => 'Fire and industrial safety (by industry)']
            ],
            [
                'code' => '05.26.05',
                'ru' => ['name' => 'Ядерная и радиационная безопасность'],
                'en' => ['name' => 'Nuclear and radiation safety']
            ],
            [
                'code' => '05.26.06',
                'ru' => ['name' => 'Химическая, биологическая и бактериологическая безопасность'],
                'en' => ['name' => 'Chemical, biological and bacteriological safety']
            ],
            [
                'code' => '05.27.01',
                'ru' => ['name' => 'Твердотельная электроника, радиоэлектронные компоненты, микро - и нано -электроника , приборы на квантовых эффектах'],
                'en' => ['name' => 'solid-State electronics, radio-electronic components, micro-and nano-electronics, devices based on quantum effects']
            ],
            [
                'code' => '05.27.02',
                'ru' => ['name' => 'Вакуумная и плазменная электроника.'],
                'en' => ['name' => 'Vacuum and plasma electronics.']
            ],
            [
                'code' => '05.27.03',
                'ru' => ['name' => 'Квантовая электроника'],
                'en' => ['name' => 'Quantum electronics']
            ],
            [
                'code' => '05.27.06',
                'ru' => ['name' => 'Технология и оборудование для производства полупроводников, материалов и приборов электронной техники'],
                'en' => ['name' => 'Technology and equipment for the production of semiconductors, materials and electronic devices']
            ],
            [
                'code' => '06.01.01',
                'ru' => ['name' => 'Общее земледелие'],
                'en' => ['name' => 'General agriculture']
            ],
            [
                'code' => '06.01.02',
                'ru' => ['name' => 'Мелиорация, рекультивация и охрана земель'],
                'en' => ['name' => 'land Reclamation, reclamation and protection']
            ],
            [
                'code' => '06.01.03',
                'ru' => ['name' => 'Агрофизика'],
                'en' => ['name' => 'Agrophysics']
            ],
            [
                'code' => '06.01.04',
                'ru' => ['name' => 'Агрохимия'],
                'en' => ['name' => 'Agrochemistry']
            ],
            [
                'code' => '06.01.05',
                'ru' => ['name' => 'Селекция и семеноводство сельскохозяйственных растений'],
                'en' => ['name' => 'Selection and seed production of agricultural plants']
            ],
            [
                'code' => '06.01.06',
                'ru' => ['name' => 'Луговодство и лекарственные, эфирно-масличные культуры'],
                'en' => ['name' => 'Grasslands and medicinal, essential-oil crops']
            ],
            [
                'code' => '06.01.07',
                'ru' => ['name' => 'Защита растений'],
                'en' => ['name' => 'plant Protection']
            ],
            [
                'code' => '06.02.01',
                'ru' => ['name' => 'Диагностика болезней и терапия животных, патология'],
                'en' => ['name' => 'Diagnostics of diseases and therapy of animals, pathology']
            ],
            [
                'code' => '06.02.02',
                'ru' => ['name' => 'Ветеринарная микробиология, вирусология, эпизоотология, микология с микотоксикологией и иммунология'],
                'en' => ['name' => 'Veterinary Microbiology, Virology, epizootology, Mycology with mycotoxicology and immunology']
            ],
            [
                'code' => '06.02.03',
                'ru' => ['name' => 'Ветеринарная фармакология с токсикологией'],
                'en' => ['name' => 'Veterinary pharmacology with toxicology']
            ],
            [
                'code' => '06.02.04',
                'ru' => ['name' => 'Ветеринарная хирургия'],
                'en' => ['name' => 'Veterinary surgery']
            ],
            [
                'code' => '06.02.05',
                'ru' => ['name' => 'Ветеринарная санитария, экология, зоогигиена и ветеринарно-санитарная экспертиза'],
                'en' => ['name' => 'Veterinary sanitation, ecology, animal hygiene and veterinary and sanitary expertise']
            ],
            [
                'code' => '06.02.06',
                'ru' => ['name' => 'Ветеринарное акушерство и биотехника репродукции животных'],
                'en' => ['name' => 'Veterinary obstetrics and Biotechnics of animal reproduction']
            ],
            [
                'code' => '06.02.07',
                'ru' => ['name' => 'Разведение, селекция и генетика сельскохозяйственных животных'],
                'en' => ['name' => 'Breeding, selection and genetics of farm animals']
            ],
            [
                'code' => '06.02.08',
                'ru' => ['name' => 'Кормопроизводство, кормление сельскохозяйственных животных'],
                'en' => ['name' => 'feed Production, feeding of farm animals']
            ],
            [
                'code' => '06.02.09',
                'ru' => ['name' => 'Звероводство и охотоведение'],
                'en' => ['name' => 'animal Husbandry and hunting']
            ],
            [
                'code' => '06.02.10',
                'ru' => ['name' => 'Частная зоотехния, технология производства продуктов'],
                'en' => ['name' => 'Private animal husbandry, technology of production of products']
            ],
            [
                'code' => '06.03.01',
                'ru' => ['name' => 'Лесные культуры, селекция, семеноводство'],
                'en' => ['name' => 'Forest crops, breeding, seed production']
            ],
            [
                'code' => '06.03.02',
                'ru' => ['name' => 'Лесоведение, лесоводство, лесоустройство и лесная таксация'],
                'en' => ['name' => 'forest science, forestry, forest management and forest taxation']
            ],
            [
                'code' => '06.03.03',
                'ru' => ['name' => 'Агролесомелиорация и защитное лесоразведение, озеленение населенных пунктов ,лесные пожары и борьба с ними'],
                'en' => ['name' => 'Agroforestry and protective afforestation, greening of settlements ,forest fires and their control']
            ],
            [
                'code' => '06.04.01',
                'ru' => ['name' => 'Рыбное хозяйство и аквакультура'],
                'en' => ['name' => 'fisheries and aquaculture']
            ],
            [
                'code' => '07.00.02',
                'ru' => ['name' => 'Отечественная история'],
                'en' => ['name' => 'Domestic history']
            ],
            [
                'code' => '07.00.03',
                'ru' => ['name' => 'Всеобщая история (соответствующего периода)'],
                'en' => ['name' => 'General history (of the corresponding period)']
            ],
            [
                'code' => '07.00.06',
                'ru' => ['name' => 'Археология'],
                'en' => ['name' => 'Archeology']
            ],
            [
                'code' => '07.00.07',
                'ru' => ['name' => 'Этнография, этнология и антропология'],
                'en' => ['name' => 'Ethnography, Ethnology and anthropology']
            ],
            [
                'code' => '07.00.09',
                'ru' => ['name' => 'Историография, источниковедение и методы исторического исследования'],
                'en' => ['name' => 'Historiography, source studies and methods of historical research']
            ],
            [
                'code' => '07.00.10',
                'ru' => ['name' => 'История науки и техники'],
                'en' => ['name' => 'History of science and technology']
            ],
            [
                'code' => '07.00.15',
                'ru' => ['name' => 'История международных отношений и внешней политики'],
                'en' => ['name' => 'History of international relations and foreign policy']
            ],
            [
                'code' => '08.00.01',
                'ru' => ['name' => 'Экономическая теория'],
                'en' => ['name' => 'Economic theory']
            ],
            [
                'code' => '08.00.05',
                'ru' => ['name' => 'Экономика и управление народным хозяйством'],
                'en' => ['name' => 'Economy and national economy management']
            ],
            [
                'code' => '08.00.10',
                'ru' => ['name' => 'Финансы, денежное обращение и кредит'],
                'en' => ['name' => 'Finance, money circulation and credit']
            ],
            [
                'code' => '08.00.12',
                'ru' => ['name' => 'Бухгалтерский учет, статистика'],
                'en' => ['name' => 'Accounting and statistics']
            ],
            [
                'code' => '08.00.13',
                'ru' => ['name' => 'Математические и инструментальные методы экономики'],
                'en' => ['name' => 'Mathematical and instrumental methods of Economics']
            ],
            [
                'code' => '08.00.14',
                'ru' => ['name' => 'Мировая экономика'],
                'en' => ['name' => 'World economy']
            ],
            [
                'code' => '09.00.01',
                'ru' => ['name' => 'Онтология и теория познания'],
                'en' => ['name' => 'Ontology and theory of knowledge']
            ],
            [
                'code' => '09.00.03',
                'ru' => ['name' => 'История философии'],
                'en' => ['name' => 'History of philosophy']
            ],
            [
                'code' => '09.00.04',
                'ru' => ['name' => 'Эстетика'],
                'en' => ['name' => 'Aesthetics']
            ],
            [
                'code' => '09.00.05',
                'ru' => ['name' => 'Этика'],
                'en' => ['name' => 'Ethics']
            ],
            [
                'code' => '09.00.07',
                'ru' => ['name' => 'Логика'],
                'en' => ['name' => 'Logic']
            ],
            [
                'code' => '09.00.08',
                'ru' => ['name' => 'Философия науки и техники'],
                'en' => ['name' => 'Philosophy of science and technology']
            ],
            [
                'code' => '09.00.11',
                'ru' => ['name' => 'Социальная философия'],
                'en' => ['name' => 'Social philosophy']
            ],
            [
                'code' => '09.00.13',
                'ru' => ['name' => 'Философия и история религии, философская антропология, философия культуры'],
                'en' => ['name' => 'Philosophy and history of religion, philosophical anthropology, philosophy of culture']
            ],
            [
                'code' => '09.00.14',
                'ru' => ['name' => 'Философия религии и религиоведение'],
                'en' => ['name' => 'Philosophy of religion and religious studies']
            ],
            [
                'code' => '10.01.01',
                'ru' => ['name' => 'Русская литература'],
                'en' => ['name' => 'Russian literature']
            ],
            [
                'code' => '10.01.02',
                'ru' => ['name' => 'Литература народов Российской Федерации (с указанием конкретной литературы)'],
                'en' => ['name' => 'Literature of the peoples of the Russian Federation (with indication of specific literature)']
            ],
            [
                'code' => '10.01.03',
                'ru' => ['name' => 'Литература народов стран зарубежья (с указанием конкретной литературы)'],
                'en' => ['name' => 'Literature of peoples of foreign countries (with indication of specific literature)']
            ],
            [
                'code' => '10.01.08',
                'ru' => ['name' => 'Теория литературы, текстология'],
                'en' => ['name' => 'Theory of literature, textology']
            ],
            [
                'code' => '10.01.09',
                'ru' => ['name' => 'Фольклористика'],
                'en' => ['name' => 'folklore Studies']
            ],
            [
                'code' => '10.01.10',
                'ru' => ['name' => 'Журналистика'],
                'en' => ['name' => 'Journalism']
            ],
            [
                'code' => '10.02.01',
                'ru' => ['name' => 'Русский язык'],
                'en' => ['name' => 'Russian language']
            ],
            [
                'code' => '10.02.02',
                'ru' => ['name' => 'Языки народов Российской федерации (с указанием конкретного языка или языковой семьи)'],
                'en' => ['name' => 'Languages of the peoples of the Russian Federation (with an indication of a specific language or language family)']
            ],
            [
                'code' => '10.02.03',
                'ru' => ['name' => 'Славянские языки'],
                'en' => ['name' => 'Slavic languages']
            ],
            [
                'code' => '10.02.04',
                'ru' => ['name' => 'Германские языки'],
                'en' => ['name' => 'German languages']
            ],
            [
                'code' => '10.02.05',
                'ru' => ['name' => 'Романские языки'],
                'en' => ['name' => 'Romance languages']
            ],
            [
                'code' => '10.02.14',
                'ru' => ['name' => 'Классическая филология, византийская и новогреческая филология'],
                'en' => ['name' => 'Classical Philology, Byzantine and modern Greek Philology']
            ],
            [
                'code' => '10.02.19',
                'ru' => ['name' => 'Теория языка'],
                'en' => ['name' => 'Theory of language']
            ],
            [
                'code' => '10.02.20',
                'ru' => ['name' => 'Сравнительно-историческое, типологическое и сопоставительное языкознание'],
                'en' => ['name' => 'Comparative-historical, typological and comparative linguistics']
            ],
            [
                'code' => '10.02.21',
                'ru' => ['name' => 'Прикладная и математическая лингвистика'],
                'en' => ['name' => 'Applied and mathematical linguistics']
            ],
            [
                'code' => '10.02.22',
                'ru' => ['name' => 'Языки народов зарубежных стран Европы, Азии, Африки'],
                'en' => ['name' => 'Languages of peoples of foreign countries of Europe, Asia, Africa']
            ],
            [
                'code' => '12.00.01',
                'ru' => ['name' => 'Теория и история права и государства; история учений о праве и государстве'],
                'en' => ['name' => 'Theory and history of law and state; history of doctrines about law and state']
            ],
            [
                'code' => '12.00.02',
                'ru' => ['name' => 'Конституционное право; конституционный судебный процесс; муниципальное право'],
                'en' => ['name' => 'Constitutional law; constitutional litigation; municipal law']
            ],
            [
                'code' => '12.00.03',
                'ru' => ['name' => 'Гражданское право предпринимательское право семейное право; международное частное право'],
                'en' => ['name' => 'Civil law business law family law; private international law']
            ],
            [
                'code' => '12.00.04',
                'ru' => ['name' => 'Финансовое право; налоговое право; бюджетное право.'],
                'en' => ['name' => 'Financial law; tax law; budget law.']
            ],
            [
                'code' => '12.00.05',
                'ru' => ['name' => 'Трудовое право; право социального обеспечения'],
                'en' => ['name' => 'Labour law; social security law']
            ],
            [
                'code' => '12.00.06',
                'ru' => ['name' => 'Земельное право; природоресурсное право; экологическое право; аграрное право'],
                'en' => ['name' => 'Land law; natural resource law; environmental law; agrarian law']
            ],
            [
                'code' => '12.00.07',
                'ru' => ['name' => 'Корпоративное право; конкурентное право; энергетическое право'],
                'en' => ['name' => 'Corporate law; competition law; energy law']
            ],
            [
                'code' => '12.00.08',
                'ru' => ['name' => 'Уголовное право и криминология; уголовно-исполнительное право'],
                'en' => ['name' => 'Criminal law and criminology; penal enforcement law']
            ],
            [
                'code' => '12.00.09',
                'ru' => ['name' => 'Уголовный процесс'],
                'en' => ['name' => 'criminal procedure']
            ],
            [
                'code' => '12.00.10',
                'ru' => ['name' => 'Международное право; Европейское право'],
                'en' => ['name' => 'International law; European law']
            ],
            [
                'code' => '12.00.11',
                'ru' => ['name' => 'Судебная деятельность, прокурорская деятельность, правозащитная и правоохранительная деятельность'],
                'en' => ['name' => 'Judicial, prosecutorial, human rights and law enforcement activities']
            ],
            [
                'code' => '12.00.12',
                'ru' => ['name' => 'Криминалистика; судебно-экспертная деятельность; оперативно-розыскная деятельность'],
                'en' => ['name' => 'Criminalistics; forensic expertise; operational and investigative activities']
            ],
            [
                'code' => '12.00.13',
                'ru' => ['name' => 'Информационное право'],
                'en' => ['name' => 'Information law']
            ],
            [
                'code' => '12.00.14',
                'ru' => ['name' => 'Административное право; административный процесс'],
                'en' => ['name' => 'Administrative law; administrative process']
            ],
            [
                'code' => '12.00.15',
                'ru' => ['name' => 'Гражданский процесс арбитражный процесс'],
                'en' => ['name' => 'Civil procedure arbitration process']
            ],
            [
                'code' => '13.00.01',
                'ru' => ['name' => 'Общая педагогика, история педагогики и образования'],
                'en' => ['name' => 'General pedagogy, history of pedagogy and education']
            ],
            [
                'code' => '13.00.02',
                'ru' => ['name' => 'Теория и методика обучения и воспитания (по областям)'],
                'en' => ['name' => 'Theory and methodology of teaching and upbringing (by area)']
            ],
            [
                'code' => '13.00.03',
                'ru' => ['name' => 'Коррекционная педагогика (сурдопедагогика и тифлопедагогика,олигофренопедагогика и логопеия)'],
                'en' => ['name' => 'Correctional pedagogy (principles of accounting and management skills,labour law and logopedia)']
            ],
            [
                'code' => '13.00.04',
                'ru' => ['name' => 'Теория и методика физического воспитания, спортивной тренировки,оздоровительной и адаптивной физической культуры)'],
                'en' => ['name' => 'Theory and methodology of physical education, sports training,Wellness and adaptive physical culture)']
            ],
            [
                'code' => '13.00.05',
                'ru' => ['name' => 'Теория, методика и организация социально-культурной деятельности'],
                'en' => ['name' => 'Theory, methodology and organization of socio-cultural activities']
            ],
            [
                'code' => '13.00.08',
                'ru' => ['name' => 'Теория и методика профессионального образования'],
                'en' => ['name' => 'Theory and methodology of professional education']
            ],
            [
                'code' => '14.01.01',
                'ru' => ['name' => 'Акушерство и гинекология'],
                'en' => ['name' => 'Obstetrics and gynecology']
            ],
            [
                'code' => '14.01.02',
                'ru' => ['name' => 'Эндокринология'],
                'en' => ['name' => 'Endocrinology']
            ],
            [
                'code' => '14.01.03',
                'ru' => ['name' => 'Болезни уха, горла и носа'],
                'en' => ['name' => 'Diseases of the ear, throat and nose']
            ],
            [
                'code' => '14.01.04',
                'ru' => ['name' => 'Внутренние болезни'],
                'en' => ['name' => 'Internal diseases']
            ],
            [
                'code' => '14.01.05',
                'ru' => ['name' => 'Кардиология'],
                'en' => ['name' => 'Cardiology']
            ],
            [
                'code' => '14.01.06',
                'ru' => ['name' => 'Психиатрия'],
                'en' => ['name' => 'Psychiatry']
            ],
            [
                'code' => '14.01.07',
                'ru' => ['name' => 'Глазные болезни'],
                'en' => ['name' => 'Eye diseases']
            ],
            [
                'code' => '14.01.08',
                'ru' => ['name' => 'Педиатрия'],
                'en' => ['name' => 'Pediatrics']
            ],
            [
                'code' => '14.01.09',
                'ru' => ['name' => 'Инфекционные болезни'],
                'en' => ['name' => 'Infectious diseases']
            ],
            [
                'code' => '14.01.10',
                'ru' => ['name' => 'Кожные и венерические болезни'],
                'en' => ['name' => 'Skin and venereal diseases']
            ],
            [
                'code' => '14.01.11',
                'ru' => ['name' => 'Нервные болезни'],
                'en' => ['name' => 'Nervous diseases']
            ],
            [
                'code' => '14.01.12',
                'ru' => ['name' => 'Онкология'],
                'en' => ['name' => 'Oncology']
            ],
            [
                'code' => '14.01.13',
                'ru' => ['name' => 'Лучевая диагностика, лучевая терапия'],
                'en' => ['name' => 'Radiation diagnostics, radiation therapy']
            ],
            [
                'code' => '14.01.14',
                'ru' => ['name' => 'Стоматология'],
                'en' => ['name' => 'Dentistry']
            ],
            [
                'code' => '14.01.15',
                'ru' => ['name' => 'Травматология и ортопедия'],
                'en' => ['name' => 'Traumatology and orthopedics']
            ],
            [
                'code' => '14.01.16',
                'ru' => ['name' => 'Фтизиатрия'],
                'en' => ['name' => 'Phthisiology']
            ],
            [
                'code' => '14.01.17',
                'ru' => ['name' => 'Хирургия'],
                'en' => ['name' => 'Surgery']
            ],
            [
                'code' => '14.01.18',
                'ru' => ['name' => 'Нейрохирургия'],
                'en' => ['name' => 'Neurosurgery']
            ],
            [
                'code' => '14.01.19',
                'ru' => ['name' => 'Детская хирургия'],
                'en' => ['name' => 'Children`s surgery']
            ],
            [
                'code' => '14.01.20',
                'ru' => ['name' => 'Анестезиология и реаниматология'],
                'en' => ['name' => 'Anesthesiology and intensive care']
            ],
            [
                'code' => '14.01.21',
                'ru' => ['name' => 'Гематология и переливание крови'],
                'en' => ['name' => 'Hematology and blood transfusion']
            ],
            [
                'code' => '14.01.22',
                'ru' => ['name' => 'Ревматология'],
                'en' => ['name' => 'Rheumatology']
            ],
            [
                'code' => '14.01.23',
                'ru' => ['name' => 'Урология'],
                'en' => ['name' => 'Urology']
            ],
            [
                'code' => '14.01.24',
                'ru' => ['name' => 'Трансплантология и искусственные органы'],
                'en' => ['name' => 'Transplantology and artificial organs']
            ],
            [
                'code' => '14.01.25',
                'ru' => ['name' => 'Пульмонология'],
                'en' => ['name' => 'Pulmonology']
            ],
            [
                'code' => '14.01.26',
                'ru' => ['name' => 'Сердечно-сосудистая хирургия'],
                'en' => ['name' => 'Cardiovascular surgery']
            ],
            [
                'code' => '14.01.27',
                'ru' => ['name' => 'Наркология'],
                'en' => ['name' => 'Narcology']
            ],
            [
                'code' => '14.01.28',
                'ru' => ['name' => 'Гастроэнтэрология'],
                'en' => ['name' => 'Gastroenterology']
            ],
            [
                'code' => '14.01.29',
                'ru' => ['name' => 'Нефрология'],
                'en' => ['name' => 'Nephrology']
            ],
            [
                'code' => '14.01.30',
                'ru' => ['name' => 'Геронтология и гериатрия'],
                'en' => ['name' => 'Gerontology and geriatrics']
            ],
            [
                'code' => '14.01.31',
                'ru' => ['name' => 'Пластическая хирургия'],
                'en' => ['name' => 'Plastic surgery']
            ],
            [
                'code' => '14.02.01',
                'ru' => ['name' => 'Гигиена'],
                'en' => ['name' => 'Hygiene']
            ],
            [
                'code' => '14.02.02',
                'ru' => ['name' => 'Эпидемиология'],
                'en' => ['name' => 'Epidemiology']
            ],
            [
                'code' => '14.02.03',
                'ru' => ['name' => 'Общественное здоровье и здравоохранение'],
                'en' => ['name' => 'Public health and healthcare']
            ],
            [
                'code' => '14.02.04',
                'ru' => ['name' => 'Медицина труда'],
                'en' => ['name' => 'occupational Medicine']
            ],
            [
                'code' => '14.02.05',
                'ru' => ['name' => 'Социология медицины'],
                'en' => ['name' => 'Sociology of medicine']
            ],
            [
                'code' => '14.02.06',
                'ru' => ['name' => 'Медико-социальная экспертиза и медико-социальная реабилитация'],
                'en' => ['name' => 'Medical and social expertise and medical and social rehabilitation']
            ],
            [
                'code' => '14.03.01',
                'ru' => ['name' => 'Анатомия человека'],
                'en' => ['name' => 'human Anatomy']
            ],
            [
                'code' => '14.03.02',
                'ru' => ['name' => 'Патологическая анатомия'],
                'en' => ['name' => 'Pathological anatomy']
            ],
            [
                'code' => '14.03.03',
                'ru' => ['name' => 'Патологическая физиология'],
                'en' => ['name' => 'Pathological physiology']
            ],
            [
                'code' => '14.03.04',
                'ru' => ['name' => 'Токсикология'],
                'en' => ['name' => 'Toxicology']
            ],
            [
                'code' => '14.03.05',
                'ru' => ['name' => 'Судебная медицина'],
                'en' => ['name' => 'Forensic medicine']
            ],
            [
                'code' => '14.03.06',
                'ru' => ['name' => 'Фармакология, клиническая фармакология'],
                'en' => ['name' => 'Pharmacology, clinical pharmacology']
            ],
            [
                'code' => '14.03.07',
                'ru' => ['name' => 'Химиотерапия и антибиотики'],
                'en' => ['name' => 'Chemotherapy and antibiotics']
            ],
            [
                'code' => '14.03.08',
                'ru' => ['name' => 'Авиационная, космическая и морская медицина'],
                'en' => ['name' => 'Aviation, space and marine medicine']
            ],
            [
                'code' => '14.03.09',
                'ru' => ['name' => 'Клиническая иммунология, аллергология'],
                'en' => ['name' => 'Clinical immunology, Allergology']
            ],
            [
                'code' => '14.03.10',
                'ru' => ['name' => 'Клиническая лабораторная диагностика'],
                'en' => ['name' => 'Clinical laboratory diagnostics']
            ],
            [
                'code' => '14.03.11',
                'ru' => ['name' => 'Восстановительная медицина, спортивная медицина, лечебная физкультура, курортология и физиотерапия'],
                'en' => ['name' => 'Restorative medicine, sports medicine, physical therapy, balneology and physiotherapy']
            ],
            [
                'code' => '14.04.01',
                'ru' => ['name' => 'Технология получения лекарств'],
                'en' => ['name' => 'Technology of obtaining medicines']
            ],
            [
                'code' => '14.04.02',
                'ru' => ['name' => 'Фармацевтическая химия, фармакогнозия'],
                'en' => ['name' => 'Pharmaceutical chemistry, pharmacognosy']
            ],
            [
                'code' => '14.04.03',
                'ru' => ['name' => 'Организация фармацевтического дела'],
                'en' => ['name' => 'organization of pharmaceutical business']
            ],
            [
                'code' => '17.00.01',
                'ru' => ['name' => 'Театральное искусство'],
                'en' => ['name' => 'Theatrical art']
            ],
            [
                'code' => '17.00.02',
                'ru' => ['name' => 'Музыкальное искусство'],
                'en' => ['name' => 'Musical art']
            ],
            [
                'code' => '17.00.03',
                'ru' => ['name' => 'Кино-, теле- и другие экранные искусства'],
                'en' => ['name' => 'Film, TV and other screen arts']
            ],
            [
                'code' => '17.00.04',
                'ru' => ['name' => 'Изобразительное и декоративно-прикладное искусство'],
                'en' => ['name' => 'Fine and decorative arts']
            ],
            [
                'code' => '17.00.05',
                'ru' => ['name' => 'Хореографическое искусство'],
                'en' => ['name' => 'Choreographic art']
            ],
            [
                'code' => '17.00.06',
                'ru' => ['name' => 'Техническая эстетика и дизайн'],
                'en' => ['name' => 'Technical aesthetics and design']
            ],
            [
                'code' => '17.00.09',
                'ru' => ['name' => 'Теория и история искусства'],
                'en' => ['name' => 'Theory and history of art']
            ],
            [
                'code' => '19.00.01',
                'ru' => ['name' => 'Общая психология, психология личности, история психологии'],
                'en' => ['name' => 'General psychology, personality psychology, history of psychology']
            ],
            [
                'code' => '19.00.02',
                'ru' => ['name' => 'Психофизиология'],
                'en' => ['name' => 'Psychophysiology']
            ],
            [
                'code' => '19.00.03',
                'ru' => ['name' => 'Психология труда, инженерная психология, эргономика'],
                'en' => ['name' => 'labor Psychology, engineering psychology, ergonomics']
            ],
            [
                'code' => '19.00.04',
                'ru' => ['name' => 'Медицинская психология'],
                'en' => ['name' => 'Medical psychology']
            ],
            [
                'code' => '19.00.05',
                'ru' => ['name' => 'Социальная психология'],
                'en' => ['name' => 'Social psychology']
            ],
            [
                'code' => '19.00.06',
                'ru' => ['name' => 'Юридическая психология'],
                'en' => ['name' => 'Legal psychology']
            ],
            [
                'code' => '19.00.07',
                'ru' => ['name' => 'Педагогическая психология'],
                'en' => ['name' => 'Pedagogical psychology']
            ],
            [
                'code' => '19.00.10',
                'ru' => ['name' => 'Коррекционная психология'],
                'en' => ['name' => 'special psychology']
            ],
            [
                'code' => '19.00.12',
                'ru' => ['name' => 'Политическая психология'],
                'en' => ['name' => 'Political psychology']
            ],
            [
                'code' => '19.00.13',
                'ru' => ['name' => 'Психология развития, акмеология'],
                'en' => ['name' => 'Psychology of development, acmeology']
            ],
            [
                'code' => '22.00.01',
                'ru' => ['name' => 'Теория, методология и история социологии'],
                'en' => ['name' => 'Theory, methodology and history of sociology']
            ],
            [
                'code' => '22.00.03',
                'ru' => ['name' => 'Экономическая социология и демография'],
                'en' => ['name' => 'Economic sociology and demography']
            ],
            [
                'code' => '22.00.04',
                'ru' => ['name' => 'Социальная структура, социальные институты и процес'],
                'en' => ['name' => 'Social structure, social institutions and processes']
            ],
            [
                'code' => '22.00.05',
                'ru' => ['name' => 'Политическая социология'],
                'en' => ['name' => 'Political sociology']
            ],
            [
                'code' => '22.00.06',
                'ru' => ['name' => 'Социология культуры, духовной жизни'],
                'en' => ['name' => 'Sociology of culture, spiritual life']
            ],
            [
                'code' => '22.00.08',
                'ru' => ['name' => 'Социология управления'],
                'en' => ['name' => 'Sociology of management']
            ],
            [
                'code' => '23.00.01',
                'ru' => ['name' => 'Теория и философия политики, история и методология'],
                'en' => ['name' => 'political Theory and philosophy, history and methodology']
            ],
            [
                'code' => '23.00.02',
                'ru' => ['name' => 'Политические институты, процессы и технологии'],
                'en' => ['name' => 'Political institutions, processes and technologies']
            ],
            [
                'code' => '23.00.03',
                'ru' => ['name' => 'Политическая культура и идеологии'],
                'en' => ['name' => 'Political culture and ideologies']
            ],
            [
                'code' => '23.00.04',
                'ru' => ['name' => 'Политические проблемы международных отношений, глобального и регионального развития'],
                'en' => ['name' => 'Political problems of international relations, global and regional development']
            ],
            [
                'code' => '23.00.05',
                'ru' => ['name' => 'Политическая регионалистика. Этнополитика'],
                'en' => ['name' => 'Political regionalism Ethnic policy']
            ],
            [
                'code' => '23.00.06',
                'ru' => ['name' => 'Конфликтология'],
                'en' => ['name' => 'Conflictology']
            ],
            [
                'code' => '24.00.01',
                'ru' => ['name' => 'Теория и история культуры'],
                'en' => ['name' => 'Theory and history of culture']
            ],
            [
                'code' => '24.00.03',
                'ru' => ['name' => 'Музееведение, консервация и реставрация историко-культурных объектов'],
                'en' => ['name' => 'Museology, conservation and restoration of historical and cultural objects']
            ],
            [
                'code' => '25.00.01',
                'ru' => ['name' => 'Общая и региональная геология'],
                'en' => ['name' => 'General and regional Geology']
            ],
            [
                'code' => '25.00.02',
                'ru' => ['name' => 'Палеонтология и стратиграфия'],
                'en' => ['name' => 'Paleontology and stratigraphy']
            ],
            [
                'code' => '25.00.03',
                'ru' => ['name' => 'Геотектоника и геодинамика'],
                'en' => ['name' => 'geotectonics and geodynamics']
            ],
            [
                'code' => '25.00.04',
                'ru' => ['name' => 'Петрология, вулканология'],
                'en' => ['name' => 'Petrology, Volcanology']
            ],
            [
                'code' => '25.00.05',
                'ru' => ['name' => 'Минералогия, кристаллография'],
                'en' => ['name' => 'Mineralogy, crystallography']
            ],
            [
                'code' => '25.00.06',
                'ru' => ['name' => 'Литология'],
                'en' => ['name' => 'lithology']
            ],
            [
                'code' => '25.00.07',
                'ru' => ['name' => 'Гидрогеология'],
                'en' => ['name' => 'Hydrogeology']
            ],
            [
                'code' => '25.00.08',
                'ru' => ['name' => 'Инженерная геология, мерзлотоведение и грунтоведение'],
                'en' => ['name' => 'Engineering Geology, permafrost and soil science']
            ],
            [
                'code' => '25.00.09',
                'ru' => ['name' => 'Геохимия, геохимические методы поисков полезных ископаемых'],
                'en' => ['name' => 'Geochemistry, geochemical methods of mineral prospecting']
            ],
            [
                'code' => '25.00.10',
                'ru' => ['name' => 'Геофизика, геофизические методы поисков полезных ископаемых'],
                'en' => ['name' => 'Geophysics, geophysical methods of mineral prospecting']
            ],
            [
                'code' => '25.00.11',
                'ru' => ['name' => 'Геология, поиски и разведка твердых полезных ископаемых'],
                'en' => ['name' => 'Geology, prospecting and exploration of solid minerals']
            ],
            [
                'code' => '25.00.12',
                'ru' => ['name' => 'Геология, поиски и разведка нефтяных и газовых месторождений'],
                'en' => ['name' => 'Geology, prospecting and exploration of oil and gas fields']
            ],
            [
                'code' => '25.00.13',
                'ru' => ['name' => 'Обогащение полезных ископаемых'],
                'en' => ['name' => 'Enrichment of minerals']
            ],
            [
                'code' => '25.00.14',
                'ru' => ['name' => 'Технология и техника геологоразведочных работ'],
                'en' => ['name' => 'Technology and techniques of geological exploration']
            ],
            [
                'code' => '25.00.15',
                'ru' => ['name' => 'Технология бурения и освоения скважин'],
                'en' => ['name' => 'well drilling and development Technology']
            ],
            [
                'code' => '25.00.16',
                'ru' => ['name' => 'Горнопромышленная и нефтегазопромысловая геология, геофизика, маркшейдерское дело и геометрия недр'],
                'en' => ['name' => 'Mining and oil and gas field Geology, Geophysics, surveying and subsurface geometry']
            ],
            [
                'code' => '25.00.17',
                'ru' => ['name' => 'Разработка и эксплуатация нефтяных и газовых месторождений'],
                'en' => ['name' => 'Development and operation of oil and gas fields']
            ],
            [
                'code' => '25.00.18',
                'ru' => ['name' => 'Технология освоения морских месторождений полезных ископаемых'],
                'en' => ['name' => 'Technology of development of offshore mineral deposits']
            ],
            [
                'code' => '25.00.19',
                'ru' => ['name' => 'Строительство и эксплуатация нефтегазоводов, баз и хранилищ'],
                'en' => ['name' => 'Construction and operation of oil and gas pipelines, bases and storage facilities']
            ],
            [
                'code' => '25.00.20',
                'ru' => ['name' => 'Геомеханика, разрушение горных пород, рудничная аэрогазодинамика и горная теплофизика'],
                'en' => ['name' => 'Geomechanics, destruction of rocks, miner aerogasdynamics and mining thermal physics']
            ],
            [
                'code' => '25.00.21',
                'ru' => ['name' => 'Теоретические основы проектирования горно-технических систем'],
                'en' => ['name' => 'Theoretical foundations of mining engineering systems design']
            ],
            [
                'code' => '25.00.22',
                'ru' => ['name' => 'Геотехнология (подземная, открытая и строительная)'],
                'en' => ['name' => 'Geotechnology (underground, open and construction)']
            ],
            [
                'code' => '25.00.23',
                'ru' => ['name' => 'Физическая география и биогеография, география почв и геохимия ландшафтов'],
                'en' => ['name' => 'Physical geography and biogeography, soil geography and landscape Geochemistry']
            ],
            [
                'code' => '25.00.24',
                'ru' => ['name' => 'Экономическая, социальная, политическая и рекреационная география'],
                'en' => ['name' => 'Economic, social, political and recreational geography']
            ],
            [
                'code' => '25.00.25',
                'ru' => ['name' => 'Геоморфология и эволюционная география'],
                'en' => ['name' => 'Geomorphology and evolutionary geography']
            ],
            [
                'code' => '25.00.26',
                'ru' => ['name' => 'Землеустройство, кадастр и мониторинг земель'],
                'en' => ['name' => 'land Management, cadastre and land monitoring']
            ],
            [
                'code' => '25.00.27',
                'ru' => ['name' => 'Гидрология суши, водные ресурсы, гидрохимия'],
                'en' => ['name' => 'land Hydrology, water resources, hydrochemistry']
            ],
            [
                'code' => '25.00.28',
                'ru' => ['name' => 'Океанология'],
                'en' => ['name' => 'Oceanology']
            ],
            [
                'code' => '25.00.29',
                'ru' => ['name' => 'Физика атмосферы и гидросферы'],
                'en' => ['name' => 'Physics of the atmosphere and hydrosphere']
            ],
            [
                'code' => '25.00.30',
                'ru' => ['name' => 'Метеорология, климатология, агрометеорология'],
                'en' => ['name' => 'Meteorology, climatology, agrometeorology']
            ],
            [
                'code' => '25.00.31',
                'ru' => ['name' => 'Гляциология и криология Земли'],
                'en' => ['name' => 'glaciology and Cryology of the Earth']
            ],
            [
                'code' => '25.00.32',
                'ru' => ['name' => 'Геодезия'],
                'en' => ['name' => 'Geodesy']
            ],
            [
                'code' => '25.00.33',
                'ru' => ['name' => 'Картография'],
                'en' => ['name' => 'Cartography']
            ],
            [
                'code' => '25.00.34',
                'ru' => ['name' => 'Аэрокосмические исследования Земли, фотограмметрия'],
                'en' => ['name' => 'aerospace studies of the Earth, photogrammetry']
            ],
            [
                'code' => '25.00.35',
                'ru' => ['name' => 'Геоинформатика'],
                'en' => ['name' => 'Geoinformatics']
            ],
            [
                'code' => '25.00.36',
                'ru' => ['name' => 'Геоэкология (по отраслям)'],
                'en' => ['name' => 'Geoecology (in branches)']
            ],
            [
                'code' => '26.00.01',
                'ru' => ['name' => 'Теология'],
                'en' => ['name' => 'Theology']
            ],
        ];
        foreach($data as $ds) {
            ListSpecial::create($ds);
        }

    }
}
