<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInputChildTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('form_input_child', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_form_input')->default(0)->index('FK_form_input_child_form_input_id')->comment('Индентификатор элемента');
			$table->string('name', 50)->comment('Имя элемента на английском');
			$table->integer('num_pp')->default(1)->comment('Номер по порядку');
			$table->string('mask')->nullable()->comment('Маска ввода');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('form_input_child');
	}

}
