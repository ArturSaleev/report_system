<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToListSpecialTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('list_special_translations', function(Blueprint $table)
		{
			$table->foreign('list_special_id')->references('id')->on('list_special')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('list_special_translations', function(Blueprint $table)
		{
			$table->dropForeign('list_special_translations_list_special_id_foreign');
		});
	}

}
