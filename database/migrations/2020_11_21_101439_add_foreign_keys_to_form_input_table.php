<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToFormInputTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('form_input', function(Blueprint $table)
		{
			$table->foreign('id_form', 'FK_form_input_form_id')->references('id')->on('form')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_input', 'FK_form_input_input_id')->references('id')->on('input')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('form_input', function(Blueprint $table)
		{
			$table->dropForeign('FK_form_input_form_id');
			$table->dropForeign('FK_form_input_input_id');
		});
	}

}
