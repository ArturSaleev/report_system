<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInputTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('form_input', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_form')->nullable()->index('FK_forms_inputs_id_form')->comment('ID формы');
			$table->integer('id_input')->nullable()->index('FK_forms_inputs_id_inputs')->comment('ID инпута');
			$table->string('input_type')->comment('Тип элемента');
			$table->integer('pos_num')->default(0)->comment('Порядковый номер на форме');
			$table->string('id_nsi')->nullable()->comment('Ссылка на классификатор');
			$table->string('class')->nullable();
			$table->string('style')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('form_input');
	}

}
