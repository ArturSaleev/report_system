<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInputValueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('form_input_value', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_form_input')->index('FK_forms_inputs_value_id_forms_input');
			$table->integer('id_form_input_child')->nullable()->index('FK_form_input_value_form_input_child_id');
			$table->text('value_string')->nullable()->default('NULL');
			$table->float('value_float')->nullable();
            $table->integer('value_integer')->nullable();
			$table->dateTime('value_date_time')->nullable();
			$table->boolean('value_boolean')->nullable();
			$table->string('lang', 10)->default('ru');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('form_input_value');
	}

}
