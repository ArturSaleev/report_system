<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToFormInputChildTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('form_input_child', function(Blueprint $table)
		{
			$table->foreign('id_form_input', 'FK_form_input_child_form_input_id')->references('id')->on('form_input')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('form_input_child', function(Blueprint $table)
		{
			$table->dropForeign('FK_form_input_child_form_input_id');
		});
	}

}
