<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInputTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('form_input_translations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('form_input_id');
			$table->string('locale')->index();
			$table->string('label');
			$table->unique(['form_input_id','locale']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('form_input_translations');
	}

}
