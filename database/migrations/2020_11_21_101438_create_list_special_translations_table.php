<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListSpecialTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_special_translations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('list_special_id');
			$table->string('locale')->index();
			$table->string('name');
			$table->unique(['list_special_id','locale']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_special_translations');
	}

}
