<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToFormInputChildTranslationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('form_input_child_translations', function(Blueprint $table)
		{
			$table->foreign('form_input_child_id')->references('id')->on('form_input_child')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('form_input_child_translations', function(Blueprint $table)
		{
			$table->dropForeign('form_input_child_translations_form_input_child_id_foreign');
		});
	}

}
