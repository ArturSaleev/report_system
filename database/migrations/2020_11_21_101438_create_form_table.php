<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('form', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_entity')->nullable()->index('FK_forms_id_entity')->comment('ID орагнизации института');
			$table->integer('id_user')->default(0)->comment('Индентификатор пользователя');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('form');
	}

}
