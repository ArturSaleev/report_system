<?php

return [
    'listForms'                 => 'Формы',
    'elementsForm'              => 'Элементы формы',

    'btnCreate'                 => 'Создать',
    'btnAdd'                    => 'Добавить',
    'btnSave'                   => 'Сохранить',
    'btnClose'                  => 'Закрыть',

    'item_editor'               => 'Редактор элементов',
    'select_item_type'          => 'Выберите тип элемента',
    'serial_number'             => 'Порядковый номер',
    'edit_enterprises'          => 'Редактор предприятий',
    'enter_company_name'        => 'Введите название предприятия',
    'form_editor'               => 'Редактор форм',
    'enter_name_form'           => 'Введите название формы',
    'select_organization'       => 'Выберите организацию',
    'enter_module_title_text'   => 'Введите текст заголовка модуля',
    'module_header'             => 'Заголовок модуля',
    'enter_title_checkbox'      => 'Введите заголовок',
    'result'                    => 'Результат',
    'select_label'              => 'Выберите 1 из справочников',
    'job_directory'             => 'Справочник должностей',
    'example'                   => 'Пример',
];
