<?php

return [
    'listForms'                 => 'Forms',
    'elementsForm'              => 'Elements',
    'btnCreate'                 => 'Create',
    'btnAdd'                    => 'Add',
    'btnSave'                   => 'Save',
    'btnClose'                  => 'Close',

    'item_editor'               => 'The item editor',
    'select_item_type'          => 'Select the item type',
    'serial_number'             => 'Serial number',
    'edit_enterprises'          => 'Editor of enterprises',
    'enter_company_name'        => 'Enter the company name',
    'form_editor'               => 'Form editor',
    'enter_name_form'           => 'Enter the name of the form',
    'select_organization'       => 'Select an organization',
    'enter_module_title_text'   => 'Enter the module title text',
    'module_header'             => 'Module header',
    'enter_title_checkbox'      => 'Enter a title for the radio button',
    'result'                    => 'Result',
    'select_label'              => 'Select 1 from the reference books',
    'job_directory'             => 'Job directory',
    'example'                   => 'Example',
];
