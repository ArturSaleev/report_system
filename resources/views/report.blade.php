@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"></div>
                <form method="post" action="forminputvalue" id="forminputvalue">
                    <div class="card-body">

                    </div>
                    <div class="card-footer" style="text-align: right">
                        <button class="btn btn-success" type="submit">Сохранить</button>
                    </div>
                    <input type="hidden" name="form_id" id="form_id" value="0" />
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script src="{{ asset('vendor/js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('vendor/js/mask.ip-input.js') }}"></script>
    <script>
        var defaultUrl = window.location.origin;

        var lang = 'ru';
        if($.cookie('Accept-Language') !== undefined){
            lang = $.cookie('Accept-Language');
        }

        $.ajaxSetup({
            async: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept-Language': lang
            },
            global: true,
            beforeSend: function(e){
                $('#load').css('display', 'block');
            },
            complete: function() {
                $('#load').css('display', 'none');
            }
        });

        //--------------------- Не относится к API ----------------------
        function getComponentsView(name, array)
        {
            var params = {"data" : array, 'onview': true};
            var cUrl = '/elements_block/';
            var ajax = $.ajax({
                method: 'POST',
                async: false,
                data: params,
                url: defaultUrl+cUrl+name,
            });
            return ajax.responseText;
        }
        //-----------------------------------------------------------------

        function returnResult(ajax)
        {
            var result = ajax.responseJSON;
            if(ajax.status == '404'){
                alert(result.message);
                return [];
            }
            return result;
        }

        function ajaxGet(page) {
            var ajax = $.ajax({
                method: 'GET',
                async: false,
                url: defaultUrl+'/api/'+page,
            });
            return returnResult(ajax);
        }

        function ajaxPost(page, params) {
            var ajax = $.ajax({
                method: 'POST',
                async: false,
                data: params,
                url: defaultUrl+'/api/'+page,
            });

            return returnResult(ajax);
        }

        var url = window.location.href.split('/');
        var id = url[url.length-1];

        $('#form_id').val(id);

        var res = ajaxGet('form/'+id);
        $('.card-body').html('');
        $('.card-header').html('<h1>'+res.data.form.name+'</h1>');

        var tabnum = 0;
        var activeBody = '.card-body';
        $.each(res.data.elements, function(i, e){
            if(e.input_type == 'tab'){
                var act = '';
                if(tabnum == 0){
                    $('.card-body').append('<ul class="nav nav-tabs tabs_header"></ul>');
                    $('.card-body').append('<div id="myTabContent" class="tab-content tabs_body"></div>');
                    act = 'active';
                }
                $('.tabs_header').append('<li class="nav-item"><a class="nav-link '+act+'" data-toggle="tab" href="#tab'+e.id+'">'+e.label+'</a></li>');
                $('.tabs_body').append('<div class="tab-pane fade show '+act+'" id="tab'+e.id+'"></div>');
                activeBody = '#tab'+e.id;
                tabnum++;
            }else {
                var html = getComponentsView(e.input_type, e, false);
                $(activeBody).append(html);
            }
            console.log(activeBody, e);
        });

        $('body').on('click', '.open_file', function(){
            var id = $(this).attr('data');
            $('input[name='+id+']').click();
        });

        $('#forminputvalue').submit(function(e){
            e.preventDefault();
            var a = $(this).serialize();
            var url = $(this).attr('action');
            var result = ajaxPost(url, a);
            console.log(result);
        })

        var resValue = ajaxGet('forminputvalue/'+id);
        $.each(resValue.data, function(i, e){
            if((e.input_type == 'checkbox') || (e.input_type == 'radio')) {
                $('#' + e.id_form_input + '_' + e.name).prop('checked', true);
            }else if(e.input_type == 'textarea') {
                $('textarea[name=' + e.name + ']').html(e.vl);
            }else if(e.input_type == 'select') {
                $('#'+e.name).val(e.vl);
            }else{
                $('input[name='+e.name+']').val(e.vl);
            }
            console.log(e);
        });

    </script>

    <style>
        .view{
            margin-bottom: 10px;
        }
    </style>
@endsection
