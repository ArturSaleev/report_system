<div class="view" data-pos="{{ $data['pos_num'] }}" id="{{ $data['id'] }}">
    @if(empty($onview))
    <button class="btn btn-inverse-danger btn-sm btn-rounded btn_menu dropdown-toggle" id="dr_{{ $data['id'] }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ $data['pos_num'] }}
    </button>
    <div class="dropdown-menu" aria-labelledby="dr_{{ $data['id'] }}">
        <span class="dropdown-item copy_module" id="{{ $data['id'] }}">Создать дубликат</span>
        <span class="dropdown-item del_module" id="{{ $data['id'] }}">Удалить</span>
    </div>
    @endif
    <div id="demo_caption">
        <div class="row">
            <div class="col-md-7">
                <label>{{ $data['form_input_children'][0]['label'] }}</label>
            </div>
            <div class="col-md-5">
                <input type="date" class="form-control" name="{{ $data['form_input_children'][0]['name'] }}">
            </div>
        </div>
    </div>

</div>
