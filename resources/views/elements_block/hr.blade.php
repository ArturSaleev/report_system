@if(empty($onview))
<div class="view" data-pos="{{ $data['pos_num'] }}" id="{{ $data['id'] }}">
    <button class="btn btn-inverse-danger btn-sm btn-rounded btn_menu dropdown-toggle" id="dr_{{ $data['id'] }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ $data['pos_num'] }}
    </button>
    <div class="dropdown-menu" aria-labelledby="dr_{{ $data['id'] }}">
        <span class="dropdown-item copy_module" id="{{ $data['id'] }}">Создать дубликат</span>
        <span class="dropdown-item del_module" id="{{ $data['id'] }}">Удалить</span>
    </div>
    <div id="demo_caption">
        <hr style="{{ $data['style'] }}" />
    </div>

</div>
@else
    <hr style="{{ $data['style'] }}" />
@endif
