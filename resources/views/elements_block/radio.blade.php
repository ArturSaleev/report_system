<div class="view" data-pos="{{ $data['pos_num'] }}" id="{{ $data['id'] }}">
    @if(empty($onview))
    <button class="btn btn-inverse-danger btn-sm btn-rounded btn_menu dropdown-toggle" id="dr_{{ $data['id'] }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ $data['pos_num'] }}
    </button>
    <div class="dropdown-menu" aria-labelledby="dr_{{ $data['id'] }}">
        <span class="dropdown-item copy_module" id="{{ $data['id'] }}">Создать дубликат</span>
        <span class="dropdown-item del_module" id="{{ $data['id'] }}">Удалить</span>
    </div>
    @endif
    <label id="demo_caption" class="h2" style="">{{ $data['label'] }}</label>
    <div>
        @foreach($data['form_input_children'] as $child)
            <div class="radio" id="{{ $child['id'] }}">
                <label>
                    <input type="radio" name="{{ $child['name'] }}" id="{{ $child['id'] }}_{{ $child['name'] }}" value="{{ $child['label'] }}">
                    {{ $child['label'] }}
                </label>
            </div>
        @endforeach
    </div>
</div>
