@foreach($data as $d)
    <li id="{{ $d['id'] }}" class="list-group-item">
        <div class="font-weight-bold text-dark mb-1">{{ $d['name'] }}
            <span class="btn btn-sm btn-outline-secondary dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog"></i></span>
            <div class="dropdown-menu">
                <a class="dropdown-item onedit" data="{{ $d['id'] }}" data-toggle="modal" data-target="#edit"><i class="fa fa-edit"></i> Редактировать</a>
                <a class="dropdown-item ondubl" data="{{ $d['id'] }}" data-toggle="modal" data-target="#dubl"><i class="fa fa-edit"></i> Создать дубликат</a>
                <a class="dropdown-item ondelete" data="{{ $d['id'] }}"><i class="fa fa-trash"></i> Удалить</a>
                <div class="dropdown-divider"></div>
                <a href="{{ asset('report/'.$d['id']) }}" target="_blank" class="dropdown-item"><i class="fa fa-eye"></i> Показать результат</a>
            </div>
        </div>
        <div class="font-weight-medium">{{ $d['entity']['name'] }}</div>
    </li>
@endforeach
