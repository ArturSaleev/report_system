@php
    $id = date("His");
@endphp
<div class="row mb-3" id="ch_{{ $id }}">
    <div class="col-lg-6">
        <input type="text" class="form-control" name="child[ru][{{ $id }}]" placeholder="{{ __('form.enter_title_checkbox') }} (ru)">
    </div>
    <div class="col-lg-6 input-group">
        <input type="text" class="form-control" name="child[en][{{ $id }}]" placeholder="{{ __('form.enter_title_checkbox') }} (en)">
        <div class="input-group-append">
            <span class="btn btn-danger el_delete" data="ch_{{ $id }}"><i class="fa fa-trash"></i></span>
        </div>
    </div>
</div>
