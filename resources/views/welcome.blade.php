@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <span class="btn btn-primary btn-sm pull-right" onclick="entity()" data-toggle="modal" data-target="#modalForms" data-id="0">
                        <i class="fa fa-plus-circle"></i> {{ __('form.btnCreate') }}
                    </span>

                    <span class="dropdown-toggle pull-left" id="lang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>
                    <div class="dropdown-menu" aria-labelledby="lang">
                        <span class="dropdown-item" onclick="SetLang('ru')">ru</span>
                        <span class="dropdown-item" onclick="SetLang('en')">en</span>
                    </div>
                    <h3>{{ __('form.listForms') }}</h3>
                </div>
                <ul class="list-group" id="listForm" style="height: 90vh; overflow: auto">

                </ul>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card-header">
                <span class="btn btn-primary btn-sm pull-right" onclick="input()"><i class="fa fa-plus-circle"></i> {{ __('form.btnAdd') }}</span>
                <h3 id="label_acitve_form">{{ __('form.elementsForm') }}</h3>
            </div>
            <div class="card-body" id="elementsBody">

            </div>
        </div>
    </div>

    <!-- Modal Form creator -->
    @include('modal.formCreate')
    @include('modal.entityCreate')
    @include('modal.elementCreate')
@endsection

@section('js')
    @parent
    <script src="{{ asset('vendor/js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('vendor/js/mask.ip-input.js') }}"></script>
    <script src="{{ asset('vendor/js/dragula.min.js') }}"></script>
    <script src="{{ asset('vendor/script.js') }}"></script>
    <script src="{{ asset('vendor/elements.js') }}"></script>
@endsection

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('vendor/css/dragula.min.css') }}">
    <style>
        #load{
            display: none;
            position: fixed;
            left: 0px;
            top:  0px;
            width: 100%;
            height: 100%;
            text-align: center;
            background-color: #c6c6c6;
        }

        #listForm li{
            cursor: pointer;
        }

        #listForm li .active{
            background-color: #007bff80;
            color: #fff;
        }

        .list-group-item .text-dark span{
            display: none;
        }

        .list-group-item.active .text-dark span{
            display: block;
        }

        li.list-group-item{
            cursor: pointer;
        }
        li.list-group-item:hover{
            background-color: #007bff3d;
            font-weight: bold;
            color: #303a40;
        }

        .list-group-item .text-dark span{
            display: none;
        }

        .list-group-item.active .text-dark span{
            display: block;
        }

        .view{
            border: solid 1px #dcdcdc;
            padding: 10px;
            border-radius: 8px;
            margin-bottom: 10px;
        }

        .btn_menu{
            position: absolute;
            left: 0px;
        }

        .radio{
            margin-left: 15px;
        }
    </style>
@endsection
