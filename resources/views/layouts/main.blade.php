<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ЦПСИ') }}</title>

    <link rel="stylesheet" href="{{ asset('vendor/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/css/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/css/style.css') }}">

    @section('css')
    @show
    <script src="{{ asset('vendor/js/vendor.bundle.base.js') }}"></script>
    <style>
        #load{
            position: fixed;
            width: 100%;
            height: 100vh;
            top: 0px;
            left: 0px;
            text-align: center;
            z-index: 1000;
            background-color: #fff;
        }
    </style>
</head>
<body>
    <div id="load">
        <img src="{{ asset('images/loading.gif') }}">
    </div>
    <div class="container">
        @yield('content')
    </div>

    <script src="{{ asset('vendor/js/off-canvas.js') }}"></script>
    <script src="{{ asset('vendor/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('vendor/js/template.js') }}"></script>
    <script src="{{ asset('vendor/js/settings.js') }}"></script>
    <script src="{{ asset('vendor/js/todolist.js') }}"></script>


    <script src="{{ asset('vendor/js/jquery.flot.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('vendor/js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendor/js/progressbar.min.js') }}"></script>
    <script src="{{ asset('vendor/js/bootstrap-datepicker.min.js') }}"></script>

    <script src="{{ asset('vendor/js/sweetalert.min.js') }}"></script>

    <script src="{{ asset('vendor/js/dashboard.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.cookie.js') }}"></script>
    @section('js')

    @show
</body>
</html>
