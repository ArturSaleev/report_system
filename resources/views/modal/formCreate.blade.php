<div class="modal fade" id="modalForms" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{ __('form.form_editor') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="form" id="formForms">
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('form.enter_name_form') }}</label>
                        <div class="row">
                            <label class="col-sm-1">ru</label>
                            <div class="col-sm-11">
                                <input type="text" class="form-control" name="name[ru]" required>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-1">en</label>
                            <div class="col-sm-11">
                                <input type="text" class="form-control" name="name[en]" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>{{ __('form.select_organization') }}</label>
                        <div class="form-group">
                            <div class="input-group">
                                <select class="form-control" name="id_entity" id="id_entity" required>
                                </select>
                                <div class="input-group-append">
                                    <span class="btn btn-dark" data-toggle="modal" data-target="#modalEntity" data-id="0"><i class="fa fa-plus-circle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="0">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" data-close="modalForms">{{ __('form.btnSave') }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('form.btnClose') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
