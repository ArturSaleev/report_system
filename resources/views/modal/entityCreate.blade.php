<div class="modal fade" id="modalEntity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{ __('form.edit_enterprises') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="entity" id="formEntity">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-3">{{ __('form.enter_company_name') }} (ru)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="name[ru]" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3">{{ __('form.enter_company_name') }} (en)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="name[en]" required>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="0">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" data-close="modalEntity">{{ __('form.btnSave') }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('form.btnClose') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
