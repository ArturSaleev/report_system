<div id="modalElements" class="modal fade show" role="dialog" aria-modal="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ __('form.item_editor') }}</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <form action="forminput" id="formSaveElements">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-9">
                            <label>{{ __('form.select_item_type') }}</label>
                            <select class="form-control" id="elementType" name="id_input">
                                <option value="">---</option>

                            </select>
                        </div>

                        <div class="col-md-2">
                            <label>{{ __('form.serial_number') }}</label>
                            <input type="number" class="form-control-sm" id="pos_num" name="pos_num" value="0">
                        </div>
                    </div>

                    <hr>
                    <div id="panel_elemets_config" class="mt-2"></div>

                    <input type="hidden" id="class" name="class" value="">
                    <input type="hidden" id="style" name="style" value="">
                    <input type="hidden" name="id_form" id="id_form" value="0">
                    <input type="hidden" name="id" id="id" value="0">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" data-close="modalElements">{{ __('form.btnSave') }}</button>
                    <a href="#" class="btn btn-default" data-dismiss="modal">{{ __('form.btnClose') }}</a>
                </div>
            </form>
        </div>
    </div>
</div>
