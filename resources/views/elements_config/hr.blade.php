<?php
/**
 * Разделитель
 *
 * Нередактируемый элемент формы, служит для визуального разделения других элементов в виде горизонтальной линии.
 * Для элемента доступна только установка цвета, добавление подчиненных элементов недоступно.
 */
?>
<div class="row">
    <div class="col-md-12">
        <label class="pull-right"></label>
        <div class="form-group mt-3">
            <div class="form-group">
                <div class="input-group mb-3">
                    <input type="color" class="form-control" id="set_color">
                </div>
            </div>
        </div>
    </div>
</div>

<label>{{ __('form.result') }}</label>
<div id="result">
    <hr style="">
</div>
<input type="hidden" name="input_type" value="hr">
