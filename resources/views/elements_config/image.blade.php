<?php
/**
 * Картика
 *
 * Нередактируемый элемент формы, служит для визуального разделения других элементов в виде загружаемой с диска пользователя картинки.
 * Добавление подчиненных элементов недоступно.
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="form-group mt-3 mb-3">
            <span class="btn btn-primary">
                <input type="file" id="load_image" accept="image/*">
            </span>
        </div>
    </div>
</div>

<label>{{ __('form.result') }}</label>
<div id="result">

</div>
<input type="hidden" name="input_type" value="image">
