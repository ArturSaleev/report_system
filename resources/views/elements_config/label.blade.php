<?php
?>
<div class="row">
    <div class="col-md-6">
        <input type="text" class="form-control" name="label[ru]" id="text_caption_ru" placeholder="{{ __('form.enter_module_title_text') }} (ru)" required>
    </div>
    <div class="col-md-6">
        <input type="text" class="form-control" name="label[en]" id="text_caption_en" placeholder="{{ __('form.enter_module_title_text') }} (en)" required>
    </div>

    <div class="col-md-12">
        <div class="form-group mt-3">
            <div class="form-group">
                <div class="btn-group">
                    <span class="btn btn-outline-secondary captfont" id="h6">1</span>
                    <span class="btn btn-outline-secondary captfont" id="h5">2</span>
                    <span class="btn btn-outline-secondary captfont" id="h4">3</span>
                    <span class="btn btn-outline-secondary captfont" id="h3">4</span>
                    <span class="btn btn-outline-secondary captfont" id="h2">5</span>
                    <span class="btn btn-outline-secondary captfont" id="h1">6</span>
                    <span class="btn btn-outline-secondary captfont" id="h0"><i class="fa fa-eraser"></i></span>
                </div>
                <div class="btn-group">
                    <span class="btn btn-outline-secondary captstyle" id="left"><i class="fa fa-align-left"></i></span>
                    <span class="btn btn-outline-secondary captstyle" id="center"><i class="fa fa-align-center"></i></span>
                    <span class="btn btn-outline-secondary captstyle" id="right"><i class="fa fa-align-right"></i></span>
                    <span class="btn btn-outline-secondary captstyle" id="justify"><i class="fa fa-align-justify"></i></span>
                </div>
            </div>
        </div>
    </div>
</div>

<label>{{ __('form.result') }}</label>
<div id="result" class="row">
    <div class="col-md-6">
        <div id="demo_caption_ru" class="well">{{ __('form.module_header') }}</div>
    </div>
    <div class="col-md-6">
        <div id="demo_caption_en" class="well">{{ __('form.module_header') }}</div>
    </div>
</div>

<input type="hidden" name="input_type" value="label">
