<?php
?>
<div class="row">
    <div class="col-md-6">
        <input type="text" class="form-control" name="label[ru]" id="text_caption_ru" placeholder="{{ __('form.enter_module_title_text') }} (ru)" required>
    </div>
    <div class="col-md-6">
        <input type="text" class="form-control" name="label[en]" id="text_caption_en" placeholder="{{ __('form.enter_module_title_text') }} (en)" required>
    </div>
</div>

<label>{{ __('form.result') }}</label>
<div id="result" class="row">
    <div class="col-md-6">
        <div id="demo_caption_ru" class="well">{{ __('form.module_header') }}</div>
    </div>
    <div class="col-md-6">
        <div id="demo_caption_en" class="well">{{ __('form.module_header') }}</div>
    </div>
</div>

<input type="hidden" name="input_type" value="file">

