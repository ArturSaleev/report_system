<?php
?>

<div class="row">
    <div class="col-md-6">
        <input type="text" class="form-control" name="label[ru]" id="text_caption_ru" placeholder="{{ __('form.enter_module_title_text') }} (ru)" required>
    </div>
    <div class="col-md-6">
        <input type="text" class="form-control" name="label[en]" id="text_caption_en" placeholder="{{ __('form.enter_module_title_text') }} (en)" required>
    </div>

    <div class="col-md-12">
        <div class="form-group mt-3">
            <div class="form-group">
                <div class="btn-group">
                    <span class="btn btn-outline-secondary captfont" id="h6">1</span>
                    <span class="btn btn-outline-secondary captfont" id="h5">2</span>
                    <span class="btn btn-outline-secondary captfont" id="h4">3</span>
                    <span class="btn btn-outline-secondary captfont" id="h3">4</span>
                    <span class="btn btn-outline-secondary captfont" id="h2">5</span>
                    <span class="btn btn-outline-secondary captfont" id="h1">6</span>
                    <span class="btn btn-outline-secondary captfont" id="h0"><i class="fa fa-eraser"></i></span>
                </div>
            </div>
        </div>
    </div>
</div>

<label>Результат</label>
<div id="result">
    <div class="row">
        <div class="col-md-6">
            <label id="demo_caption_ru" class="well">{{ __('form.module_header') }}</label>
            <br />
            <label id="demo_caption_en" class="well">{{ __('form.module_header') }}</label>
        </div>
        <div class="col-md-6 mb-3">
            <input type="text" class="form-control" id="input_mask" data-inputmask="">
            <br>
            <select class="form-control set_input_mask">
                <option value="">---</option>
                <option value="+9 (999) 999-99-99">Mobile +9 (999) 999-99-99</option>
                <option value="8 (9999) 99-99-99">Phone 8 (9999) 99-99-99</option>
                <option value="9999-9999-9999-9999">Cart 9999-9999-9999-9999</option>
                <option value="ip-mask">Ip Address 127.0.0.1</option>
            </select>
        </div>
    </div>
</div>
<input type="hidden" name="mask" value="">
<input type="hidden" name="input_type" value="inputMask">
