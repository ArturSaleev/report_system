<?php
?>
<div class="row">
    <div class="col-md-4">
        <input type="text" class="form-control" name="label[ru]" id="text_caption_ru" placeholder="{{ __('form.enter_module_title_text') }} (ru)" required>
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" name="label[en]" id="text_caption_en" placeholder="{{ __('form.enter_module_title_text') }} (en)" required>
    </div>
    <div class="col-md-4">
        <div class="input-group-append">
            <span class="btn btn-outline-secondary captfont" id="h6">1</span>
            <span class="btn btn-outline-secondary captfont" id="h5">2</span>
            <span class="btn btn-outline-secondary captfont" id="h4">3</span>
            <span class="btn btn-outline-secondary captfont" id="h3">4</span>
            <span class="btn btn-outline-secondary captfont" id="h2">5</span>
            <span class="btn btn-outline-secondary captfont" id="h1">6</span>
            <span class="btn btn-outline-secondary captfont" id="h0"><i class="fa fa-eraser"></i></span>
        </div>
    </div>
</div>

<label>{{ __('form.result') }}</label>
<div id="result">
    <div class="row">
        <div class="col-md-6">
            <label id="demo_caption_ru" class="well">{{ __('form.module_header') }}</label>
        </div>
        <div class="col-md-6 mb-3">
            <label id="demo_caption_en" class="well">{{ __('form.module_header') }}</label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label>{{ __('form.select_label') }}</label>
        </div>
        <div class="col-md-6 mb-3">
            <select class="form-control" name="id_nsi" id="select_child" required>
                <option value="">-- Не выбрано --</option>
                <option value="listspecial">{{ __('form.job_directory') }}</option>
            </select>
        </div>
    </div>
    <div class="row">
        <label class="col-md-6">{{ __('form.example') }}</label>
        <select class="form-control" id="test_select"></select>
    </div>
</div>

<input type="hidden" name="input_type" value="select">
