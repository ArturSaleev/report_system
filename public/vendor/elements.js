$(function(){
    var body = $('body');

    body.on('keyup', '#text_caption_ru', function(){
        var val = $(this).val();
        $('#result #demo_caption_ru').html(val);
    });

    body.on('keyup', '#text_caption_en', function(){
        var val = $(this).val();
        $('#result #demo_caption_en').html(val);
    });

    body.on('click', '.captfont', function(){
        var id = $(this).attr('id');
        $('#class').val(id);
        $('.captfont.active').removeClass('active');
        $('#result #demo_caption_ru').attr('class', id);
        $('#result #demo_caption_en').attr('class', id);
        $(this).addClass('active');
    });

    body.on('click', '.captstyle', function(){
        var id = $(this).attr('id');
        var style = 'text-align: '+id+';';
        $('#style').val(style);
        $('.captstyle.active').removeClass('active');
        $('#result #demo_caption_ru').attr('style', style);
        $('#result #demo_caption_en').attr('style', style);
        $(this).addClass('active');
    });

    body.on('change', '#load_image', function(){
        console.log($(this).files);
        if (this.files && this.files[0]) {
            var FR = new FileReader();
            FR.addEventListener("load", function(e) {
                var img = '<img src="'+e.target.result+'" width="100px">'+
                    '<input type="hidden" name="child[]" value="'+e.target.result+'">';
                $('#result').append(img);
            });

            FR.readAsDataURL( this.files[0] );
        }
    })

    body.on('click', '.new_checkbox', function(){
        var html = getComponentsView('el_checkbox', []);
        $('#result').append(html);
    });

    body.on('click', '.el_delete', function(){
        var id = $(this).attr('data');
        $('#'+id).remove();
    })

    body.on('click', '.open_file', function(){
        var id = $(this).attr('data');
        $('input[name='+id+']').click();
    });

    body.on('change', '#set_color', function(){
        var val = $(this).val();
        $('#result').children('hr').css('background-color', val);
        $('#style').val('background-color: '+val);
    })

    body.on('change', '.set_input_mask', function(){
        var inp_mask_save = $('input[name=mask]');
        var inp_mask = $('#input_mask');
        var val = $(this).val();

        inp_mask_save.val('');

        inp_mask.unmask();
        if(val !== 'ip-mask') {
            inp_mask.mask(val);
            inp_mask.attr('placeholder', val);
            inp_mask.attr('class', 'form-control mask');
            inp_mask.attr('class', 'form-control data-mask', val);
        }else {
            inp_mask.attr('class', 'form-control ip-mask');
            inp_mask.attr('placeholder', '000.000.000.000');
            $('.ip-mask').ipAddress();
        }
        inp_mask_save.val(val);
    });
})

var drake = dragula([document.getElementById("elementsBody")])
    .on('out', function () {
        var s = [];
        $('.view').each(function(){
            var id = $(this).attr('id');
            s.push(id);
        });
        var result = ajaxPost('refreshposition', {'form_id' : activeForm.id, 'positions' : s});
        $.each(result.data, function(i, e){
            $('.view#'+e.id).attr('data-pos', e.pos_num);
            $('#dr_'+e.id).html(e.pos_num);
        });
    });
