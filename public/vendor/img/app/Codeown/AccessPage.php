<?php     
    use Illuminate\Support\Facades\DB;
       
	function OnAccess($id_user)
    {
        $currentPath = Request::path();
        if($currentPath == '/'){return true;}                  
        if($currentPath == 'home'){return true;}
        
        $q = DB::select("select * from users where id = $id_user");
        $id_role = $q[0]->id_role;        
        if($id_role == 0){
            return true;
        }
        
        $qs = DB::select("SELECT COUNT(*) cnt FROM access_modules am, diс_modules dm 
        WHERE dm.id = am.id_module
        AND am.id_role = ?
        AND dm.url = ?", [$id_role, $currentPath]
        );
        
        return false;
    }
    