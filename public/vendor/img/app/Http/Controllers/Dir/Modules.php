<?php

namespace App\Http\Controllers\Dir;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Modules extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $q = DB::select("select * from dic_modules");  
        $routes = \Route::getRoutes();
        
        $ds = array();
        $puns = array("/", "api/user", "login", "logout", "register", "password/reset", "password/email", "password/reset/{token}", "password/reset", "home");
        
        
        foreach($routes as $link){            
            $url = trim($link->uri);
            $b = true;    
            foreach($puns as $p){
                if(trim($p) == $url){
                    $b = false;
                }
            }
            
            foreach($ds as $p){
                if(trim($p) == $url){
                    $b = false;
                }
            }
            
            foreach($q as $ps){
                if(trim($ps->url) == $url){
                    $b = false;
                }
            }
            
            if($b == true){
                array_push($ds, $url);
            }
        }
                                
        return view("dir/modules", ["list"=>$q, "routes"=>$ds]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($name, $url, $desc)
    {
        $id_user = Auth::id();      
        
        DB::table('dic_modules')->insert(
            ['name' => $name, 'url' => $url, 'descr' => $desc, 'id_user'=>$id_user]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name   = $request->input('module_name');
        $id     = $request->input('module_id');
        $url    = $request->input('module_url');
        $desc   = $request->input('module_desc');  
        if($id == 0){
            $this->create($name, $url, $desc);
        }
        
        return redirect()->to('dir/modules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('dic_modules')->delete(['id' => $id]);
        return redirect()->to('dir/modules');
    }
}
