<?php

namespace App\Http\Controllers\Dir;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Roles extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $q = DB::table('dic_roles')->get();
        return view('dir/roles', ["list"=>$q]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($name)
    {        
        DB::table('dic_roles')->insert(
            ['name' => $name]
        );        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name   = $request->input('role_name');
        $id     = $request->input('role_id');               
                 
        if($id == 0){            
            $this->create($name);
        }else{
            $this->edit($id, $name);
        }
        
        return redirect()->to('dir/roles');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dan['modules'] = DB::select('SELECT dm.id, dm.name,dm.descr, 
        (SELECT COUNT(*) FROM access_modules am WHERE am.id_module = dm.id AND am.id_role = ?) act 
        FROM dic_modules dm ORDER BY dm.name', [$id]);
        
        $dan['types'] = DB::select("SELECT dot.id, dot.name,
        (SELECT COUNT(*) FROM access_types at WHERE at.id_type = dot.id AND at.id_role = ?) act
        FROM dic_object_type dot ORDER BY id", [$id]);
        
        $ds = array();
        $q = DB::table('dic_country')->get();        
        foreach($q as $k=>$v){
            foreach($v as $t=>$d){
                $ds[$k][$t] = $d;
            }
            
            $r = DB::select('select * from dic_region where id_country = ?', [$v->code]);
            $reg = array();            
            foreach($r as $rk=>$rv){
                foreach($rv as $t=>$d){
                    $reg[$rk][$t] = $d;
                }
                $reg[$rk]['city'] = DB::select('select d.id, d.name,
                 (SELECT COUNT(*) FROM access_cities at WHERE at.id_city = d.id AND at.id_role = ?) act
                from dic_cities d where d.id_country = ? and d.id_region = ?', [$id, $v->code, $rv->id]);
            }
            
            $ds[$k]['region'] = $reg;
        }
        $dan['city'] = $ds;
        
        return response()->json($dan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $name)
    {
        DB::table('dic_roles')
        ->where('id', $id)
        ->update(
            ['name' => $name]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
