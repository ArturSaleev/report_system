var defaultUrl = window.location.origin;
var body = $('body');

var lang = 'ru';
if($.cookie('Accept-Language') !== undefined){
    lang = $.cookie('Accept-Language');
    $.cookie('Accept-Language', lang);
}
$('#lang').html(lang);

$.ajaxSetup({
    async: false,
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Accept-Language': lang
    },
    global: true,
    beforeSend: function(e){
        $('#load').css('display', 'block');
    },
    complete: function() {
        $('#load').css('display', 'none');
    }
});

function returnResult(ajax)
{
    var result = ajax.responseJSON;
    if(ajax.status == '404'){
        alert(result.message);
        return [];
    }
    return result;
}

function ajaxPost(page, params) {
    var ajax = $.ajax({
        method: 'POST',
        async: false,
        data: params,
        url: defaultUrl+'/api/'+page,
    });

    return returnResult(ajax);
}

function ajaxPut(page, params) {
    var ajax = $.ajax({
        method: 'PUT',
        async: false,
        data: params,
        url: defaultUrl+'/api/'+page,
    });
    return returnResult(ajax);
}

function ajaxGet(page) {
    var ajax = $.ajax({
        method: 'GET',
        async: false,
        url: defaultUrl+'/api/'+page,
    });
    return returnResult(ajax);
}

function ajaxDelete(page) {
    var ajax = $.ajax({
        method: 'DELETE',
        async: false,
        url: defaultUrl+'/api/'+page,
    });
    return returnResult(ajax);
}

//------------------Запросы не относящиеся к API -------------------------------------
function SetLang(lang)
{
    $.cookie('Accept-Language', lang);
    window.location.reload();
}


function getComponentsView(name, array, onComponent = true)
{
    var params = {"data" : array};
    var cUrl = '/components/';
    if(onComponent == false){
        cUrl = '/elements_block/';
    }
    var ajax = $.ajax({
        method: 'POST',
        async: false,
        data: params,
        url: defaultUrl+cUrl+name,
    });
    return ajax.responseText;
}

function getElementConfig(name)
{
    var ajax = $.ajax({
        method: 'GET',
        async: false,
        url: defaultUrl+'/elements_config/'+name,
    });
    return ajax.responseText;
}

//----------------------------------------------------------------------------------
var activeForm;

function entity(){
    $('#id_entity').html('');
    var result = ajaxGet('entity');
    if(result.data.length > 0) {
        $.each(result.data, function(i, e){
            $('#id_entity').append(new Option(e.name, e.id));
        });
    }
};

function form(){
    $('#listForm').html('');
    var result = ajaxGet('form');
    if (result.data.length > 0) {
        var html = getComponentsView('list_form', result.data);
        $('#listForm').html(html);
    }
};

function clearModalElement()
{
    $('#panel_elemets_config').html('');
    $('#id').val('0');
    $('#style').val('');
    $('#class').val('');
    $('#pos_num').val('0');
    $('#elementType').val('0');
}

function showForm(id)
{
    var result = ajaxGet('form/'+id);
    activeForm = result.data.form;
    clearModalElement();

    $('#elementsBody').html('');
    $('#id_form').val(activeForm.id);
    $('#label_acitve_form').html(activeForm.name);
    $.each(result.data.elements, function(i, e){
        var html = getComponentsView(e.input_type, e, false);
        $('#elementsBody').append(html);
    })
}

function input()
{
    if(!activeForm){
        alert('Выберите форму!');
        return false;
    }
    $('#elementType').html('');
    $('#elementType').append(new Option('---', '0'));
    var result = ajaxGet('input');
    $.each(result, function(i, e){
        $('#elementType').append(new Option(e.label, e.id));
    });

    $('#panel_elemets_config').html('');
    $('#modalElements').modal('show');
}

function forminput()
{
    var id = $('#id_form').val();
    showForm(id);
}

//----------------------------------------------------------------------------------

$(function(){
    form();

    body.on('click', 'span', function(){
       var ifModal = $(this).data('toggle');
       if(ifModal !== undefined){
           var target = $(this).data('target'),
               id = $(this).data('id');

           var nameVal = $(this).data('name');

           $(target+' :input[name=id]').val(id);
           $(target+' :input[name=name]').val('');
           if(nameVal !== undefined){
               $(target+' :input[name=name]').val(nameVal);
           }
       }
    });

    body.on('click', '.save', function(){
        var formId = $(this).data('save');
        $('#'+formId).submit();
    });

    body.on('submit', 'form', function(e){
        e.preventDefault();

        var id = 0;

        var s = $(this).serializeArray();
        $.each(s, function(i, e){
            if(e.name == 'id' && e.value !== '0'){
                id = e.value;
            }
        });

        var array = $(this).serialize();
        var action = $(this).attr('action');

        var result = [];
        if(id === 0) { //Insert
            result = ajaxPost(action, array);
        }else{ //Update
            result = ajaxPut(action+'/'+id, array);
        }

        var fn = window[action];
        if (typeof fn === "function") fn();

        var id = $(this).attr('id');
        var bt = $('#'+id+' :button[type=submit]');

        $.each(bt, function(){
            var closeModal = $(this).data('close');
            if(closeModal !== undefined){
                $('#'+closeModal).modal('toggle');
            }
        });

        //

    });

    body.on('click', '#listForm li', function(){
        var id = $(this).attr('id');
        if($('#listForm li.active').attr('id') !== id) {
            $('#listForm li.active').removeClass('active');
            $(this).addClass('active');
            showForm(id);
        }
    });

    body.on('click', '.onedit', function(){
        if(activeForm.id == $(this).attr('data')){
            entity();
            $.each(activeForm.translations, function(i, e){
                $('#formForms :input[name="name['+e.locale+']"]').val(e.name);
            });

            $('#formForms :input[name=id_entity]').val(activeForm.id_entity);
            $('#formForms :input[name=id]').val(activeForm.id);
            $('#modalForms').modal('show');
        }
    });

    body.on('click', '.ondelete', function(){
        var id = $(this).attr('data');
        if(confirm('Удалить выбранный макет формы?')){
            ajaxDelete('form/'+id);
            form();
        }
    });

    body.on('change', '#elementType', function(){
        var id = $(this).val();
        var result = ajaxGet('input/'+id);
        console.log(result);

        var html = getElementConfig(result.name);
        $('#panel_elemets_config').html(html);
    });

    body.on('click', '.del_module', function(){
        var id = $(this).attr('id');
        var res = ajaxDelete('forminput/'+id);
        if(res.success){
            $('.view#'+id).remove();
        }
    });

    body.on('change', '#select_child', function(){
        var val = $(this).val();
        if(val.trim() == ''){
            alert('Нельзя выбирать пустое значение!')
            return false;
        }

        var res = ajaxGet('getservice/'+val);
        console.log(res);
        $('#test_select').html('');
        $.each(res, function(i, e){
            $('#test_select').append(new Option(e.name, e.id));
        });
    });
});
