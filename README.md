# Генератор форм

### Установка
- Скачать исходники
- Запустить `composer install`
- В настройка `.env` установить подключение к базе данных

`Запустить`
- php artisan make:migration
- php artisan db:seed

`Или`
- php artisan db:seed --class=InsertInputs
- php artisan db:seed --class=InsertHR
- Запустить сервер `php artisan serve` 

### Схема построения базы данных
<img src="./public/images/graph.jpg">

### Дополнительные настройки
- в php.ini установить max_input_vars = 10000 или более
