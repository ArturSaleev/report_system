<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;
use App\Http\Controllers\EntityController;
use App\Http\Controllers\FormInputController;
use App\Http\Controllers\InputController;
use App\Http\Controllers\FormInputValueController;
use App\Http\Controllers\ServiceController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('/form', FormController::class);
Route::resource('/entity', EntityController::class);
Route::resource('/input', InputController::class);
Route::resource('/forminput', FormInputController::class);
Route::resource('/forminputvalue', FormInputValueController::class);

Route::post('/refreshposition', [FormInputController::class, 'RefreshPosition']);

//Отдельный запрос реализованный как сервис запроса на поле выпадающего списка
Route::get('/getservice/{name}', [ServiceController::class, 'getService']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
