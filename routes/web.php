<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ComponentsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ComponentsController::class, 'Welcome']);
Route::get('/report/{id}', [ComponentsController::class, 'Report']);


Route::post('/components/{page}',  [ComponentsController::class, 'index']);
Route::get('/elements_config/{name}', [ComponentsController::class, 'elementConfig']);
Route::post('/elements_block/{page}',  [ComponentsController::class, 'elementsBlock']);
Route::get('/image/{filename}', [ComponentsController::class, 'viewImage']);

