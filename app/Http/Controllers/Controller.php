<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $lang;
    public function __construct(Request $request)
    {
        $this->lang = App::getLocale();
        if($request->hasHeader('Accept-Language')) {
            $this->lang = $request->header('Accept-Language');
            App::setLocale($this->lang);
            session()->put('locale', $this->lang);
        }
    }
}
