<?php

namespace App\Http\Controllers;

use App\Models\FormInput;
use App\Models\FormInputChild;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FormInputController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'input_type' => 'required',
            'id_form' => 'required',
        ]);

        $posnum = $request->pos_num;
        if((int) $posnum == 0){
            try {
                $posnum = FormInput::where('id_form', $request->id_form)->max('pos_num')+1;
            }catch(\Exception $e){
                $posnum++;
            }
        }

        //return $this->sendError(json_encode($request->all()));

        $data = [
            "id_input" => $request->id_input,
            "id_form" => $request->id_form,
            "class" => $request->class,
            "style" => $request->style,
            "input_type" => $request->input_type,
            "pos_num" => $posnum
        ];

        if(isset($request->id_nsi)){
            $data['id_nsi'] = $request->id_nsi;
        }

        if(isset($request->label['ru'])){
            $data['ru'] = ["label" => $request->label['ru']];
        }

        if(isset($request->label['en'])){
            $data['en'] = ["label" => $request->label['en']];
        }

        try {
            $res = FormInput::create($data);
        }catch(\Exception $e){
            return $this->sendError($e->getMessage());
        }

        $id = $res->id;

        if(!in_array($request->input_type, ['label', 'hr', 'tab'])) {
            if ($request->has('child')) {
                if(isset($request->child['ru'])){
                    $i = 0;
                    foreach ($request->child['ru'] as $k=>$child) {
                        $save = $this->SaveInputChild($id, $request->input_type, $i + 1, $child, $request->child['en'][$k]);
                        if(!$save){
                            return $this->sendError($save);
                        }
                        $i++;
                    }
                }else {
                    foreach ($request->child as $k => $child) {
                        $child_label = $child;
                        if ($request->input_type == 'image') {
                            $child_label = $this->SaveImage($child);
                        }
                        $this->SaveInputChild($id, $request->input_type, $k + 1, $child_label, $child_label);
                    }
                }
            } else {
                $label_ru = (isset($request->label['ru'])) ? $request->label['ru'] : $request->label;
                $label_en = (isset($request->label['en'])) ? $request->label['en'] : $request->label;
                $mask = ($request->mask) ? $request->mask : null;

                $this->SaveInputChild($id, $request->input_type, 1, $label_ru, $label_en, $mask);

            }
        }
        return $this->show($id);
    }

    private function SaveInputChild($id_form, $type, $num_pp, $label_ru, $label_en, $mask = null)
    {
        $name = substr($type, 0, 2) . '_' . date("YmdHis");
        $data = [
            "id_form_input" => $id_form,
            "name" => $name,
            "num_pp" => $num_pp,
            "ru" => ["label" => $label_ru],
            "en" => ["label" => $label_en]
        ];
        if($mask !== null){
            $data['mask'] = $mask;
        }

        try {
            FormInputChild::create($data);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return true;
    }

    private function SaveImage($base64)
    {
        $s = explode(';', $base64);
        $type = str_replace('data:image/', '', $s[0]);
        $ds = date("dmYHis");
        if (preg_match('/^data:image\/(\w+);base64,/', $base64)) {
            $data = substr($base64, strpos($base64, ',') + 1);
            $data = base64_decode($data);
            Storage::disk('local')->put("$ds.$type", $data);
            return "$ds.$type";
        }else{
            return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formInput = FormInput::with('formInputChildren')->find($id);
        return $this->sendResponse($formInput);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $formInput = FormInput::find($id);
        $form_id = $formInput->id_form;

        try {
            $formInputChild = FormInputChild::where('id_form_input', $id)->get();
            FormInputChild::where('id_form_input', $id)->delete();
        }catch(\Exception $e){
            return $this->sendError('Выбранный Вами объект удалять запрещено! Имеются зависимости!');
        }

        if($formInput->input_type == 'image'){
            foreach($formInputChild as $fic){
                Storage::delete($fic->label);
            }
        }

        $formInput->delete();
        $formInputNew = FormInput::where('id_form', $form_id)->get();
        foreach($formInputNew as $k=>$v){
            FormInput::where('id', $v->id)->update(['pos_num' => $k+1]);
        }

        return  $this->sendResponse([]);
    }


    public function RefreshPosition(Request $request)
    {
        $form_id = $request->form_id;

        foreach($request->positions as $k=>$v){
            FormInput::where('id', $v)->update(['pos_num' => $k+1]);
        }
        $result = FormInput::where('id_form', $form_id)->get(['id', 'pos_num']);
        return $this->sendResponse($result);
    }
}
