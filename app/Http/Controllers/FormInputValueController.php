<?php

namespace App\Http\Controllers;

use App\Models\FormInput;
use App\Models\FormInputChild;
use App\Models\FormInputValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormInputValueController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form_id = $request->form_id;
        $d = $request->all();
        unset($d['form_id']);

        FormInputValue::where('id_form_input', $form_id)->delete();

        foreach($d as $name => $data){
            $formInputChild = FormInputChild::with('formInput')->where('name', $name)->first();
            $id_child = $formInputChild->id;
            $type = $formInputChild->formInput->input_type;
            if(is_array($data)){
                foreach($data as $value) {
                    $res = $this->SaveValue($form_id, $id_child, $value, $type);
                }
            }else {
                $res = $this->SaveValue($form_id, $id_child, $data, $type);
            }
        }
        return $this->sendResponse([], 'Данные сохранены успешно');
    }

    private function SaveValue($id_form, $id_child, $value, $type)
    {
        $formInputValue = new FormInputValue();
        $formInputValue->id_form_input = $id_form;
        $formInputValue->id_form_input_child = $id_child;
        $formInputValue->lang = $this->lang;

        if(in_array($type, ['inputMask', 'inputString', 'textarea', 'file'])) {
            $formInputValue->value_string = $value;
        }

        if($type == 'inputNumber') {
            $formInputValue->value_float = $value;
        }

        if(in_array($type, ['radio', 'checkbox'])) {
            $formInputValue->value_string = $value;
            $formInputValue->value_boolean = true;
        }

        if($type == 'inputDate'){
            $formInputValue->value_date_time = $value;
        }

        if($type == 'select'){
            $formInputValue->value_integer = $value;
        }

        return $formInputValue->saveOrFail();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formInputChild = DB::select("SELECT
            fic.name,
            COALESCE(cast(fiv.value_boolean as text), cast(fiv.value_float as text), cast(fiv.value_integer as text), to_char(fiv.value_date_time, 'YYYY-MM-DD'), fiv.value_string) vl,
            fic.id_form_input,
            fi.input_type
        FROM
            form_input_value fiv,
            form_input_child fic,
            form_input fi
        where
            fic.id = fiv.id_form_input_child
            and fic.id_form_input = fi.id
            and fi.id_form = $id");

        return $this->sendResponse($formInputChild);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
