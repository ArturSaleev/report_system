<?php

namespace App\Http\Controllers;

use App\Models\ListSpecial;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function __construct()
    {
        //
    }

    public function getService(Request $request)
    {
        if(empty($request->name)){
            return [];
        }

        $name = $request->name;
        if(method_exists($this, $name)){
            return response()->json($this->$name());
        }
        return [];
    }

    public function listspecial()
    {
        return ListSpecial::all();
    }
}
