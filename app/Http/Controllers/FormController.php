<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\FormInput;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ServiceController;

class FormController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form = Form::with('entity')->orderBy('id')->get();
        //DB::enableQueryLog();
        //dd(DB::getQueryLog());
        return $this->sendResponse($form);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'id_entity' => 'required',
            'id' => 'required'
        ]);

        $data = [
            "name" => $request->name['ru'],
            "id_entity" => $request->id_entity,
            "ru" => ["name" => $request->name['ru']],
            "en" => ["name" => $request->name['en']],
        ];

        try{
            Form::create($data);
        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse([]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result['form'] = Form::find($id);
        $formInput = FormInput::with('formInputChildren')
            ->where('id_form', $id)
            ->orderBy('pos_num')
            ->get();


        foreach($formInput as $k=>$v){
            if($v->input_type == 'select'){
                $method = $v->id_nsi;
                $res = (new ServiceController())->$method();
                $formInput[$k]['params_id_ns'] = $res;
            }
        }

        $result['elements'] = $formInput;

        return $this->sendResponse($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
            "name" => $request->name['ru'],
            "id_entity" => $request->id_entity,
            "ru" => ["name" => $request->name['ru']],
            "en" => ["name" => $request->name['en']],
        ];

        try{
            $form = Form::find($id);
            $form->update($data);
        }catch (\Exception $e){
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Form::find($id)->delete();
        }catch(Exception $e){
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse([]);
    }
}
