<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ComponentsController extends Controller
{
    public function __construct(Request $request)
    {
        if($request->cookie('Accept-Language')) {
            $lang = $request->cookie('Accept-Language');
            app()->setLocale($lang);
            session()->put('locale', $lang);
        }
    }

    //-----------------------------------------------------------------------------------------
    public function Welcome()
    {
        return view('welcome');
    }

    public function Report($id)
    {
        return view('report');
    }
    //-----------------------------------------------------------------------------------------

    public function index(Request $request, $page)
    {
        $array = $request->all();
        return view('components/'.$page, $array);
    }

    public function elementConfig($name)
    {
        return view('elements_config/'.$name);
    }

    public function elementsBlock(Request $request, $page)
    {
        $array = $request->all();
        return view('elements_block/'.$page, $array);
    }

    public function viewImage($filename)
    {
        $path = Storage::disk('local')->path($filename);
        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

}
