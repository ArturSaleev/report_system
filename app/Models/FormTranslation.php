<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'content'];
}
