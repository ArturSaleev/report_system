<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormInputTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['label', 'content'];
}
