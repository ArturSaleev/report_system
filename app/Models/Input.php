<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

/**
 * @property int $id
 * @property string $name
 * @property string $label
 * @property string $descript
 * @property string $created_at
 * @property string $updated_at
 * @property FormInput[] $formInputs
 */
class Input extends Model implements TranslatableContract
{
    use Translatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'input';

    public $translatedAttributes = ['label', 'descript', 'content'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'label', 'descript', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formInputs()
    {
        return $this->hasMany('App\Models\FormInput', 'id_input');
    }

}
