<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class ListSpecial extends Model implements TranslatableContract
{
    use Translatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'list_special';

    public $translatedAttributes = ['name'];

    /**
     * @var array
     */
    protected $fillable = ['code', 'name', 'created_at', 'updated_at'];
}
