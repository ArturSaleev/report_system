<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

/**
 * @property int $id
 * @property int $id_form
 * @property int $id_input
 * @property string $input_type
 * @property string $label
 * @property int $pos_num
 * @property string $id_nsi
 * @property string $created_at
 * @property string $updated_at
 * @property string $class
 * @property string $style
 * @property Form $form
 * @property Input $input
 * @property FormInputChild[] $formInputChildren
 * @property FormInputValue[] $formInputValues
 */
class FormInput extends Model implements TranslatableContract
{
    use Translatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'form_input';

    public $translatedAttributes = ['label', 'content'];
    /**
     * @var array
     */
    protected $fillable = ['id_form', 'id_input', 'input_type', 'label', 'pos_num', 'id_nsi', 'class', 'style', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo('App\Models\Form', 'id_form');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function input()
    {
        return $this->belongsTo('App\Models\Input', 'id_input');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formInputChildren()
    {
        return $this->hasMany('App\Models\FormInputChild', 'id_form_input');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formInputValues()
    {
        return $this->hasMany('App\Models\FormInputValue', 'id_form_input');
    }
}
