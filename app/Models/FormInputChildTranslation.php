<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormInputChildTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['label', 'content'];
}
