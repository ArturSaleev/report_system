<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

/**
 * @property int $id
 * @property int $id_form_input
 * @property string $name
 * @property string $label
 * @property int $num_pp
 * @property string $created_at
 * @property string $updated_at
 * @property string $mask
 * @property FormInput $formInput
 * @property FormInputValue[] $formInputValues
 */
class FormInputChild extends Model implements TranslatableContract
{
    use Translatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'form_input_child';

    public $translatedAttributes = ['label', 'content'];

    /**
     * @var array
     */
    protected $fillable = ['id_form_input', 'name', 'label', 'num_pp', 'mask', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formInput()
    {
        return $this->belongsTo('App\Models\FormInput', 'id_form_input');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formInputValues()
    {
        return $this->hasMany('App\Models\FormInputValue', 'id_form_input_child');
    }
}
