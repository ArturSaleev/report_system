<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

/**
 * @property int $id
 * @property int $id_entity
 * @property string $name
 * @property int $id_user
 * @property string $created_at
 * @property string $updated_at
 * @property Entity $entity
 * @property FormInput[] $formInputs
 */
class Form extends Model implements TranslatableContract
{
    use Translatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'form';

    public $translatedAttributes = ['name', 'content'];

    /**
     * @var array
     */
    protected $fillable = ['id_entity', 'name', 'id_user', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo('App\Models\Entity', 'id_entity');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formInputs()
    {
        return $this->hasMany('App\Models\FormInput', 'id_form');
    }

}
