<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InputTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['label', 'descript', 'content'];
}
