<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_form_input
 * @property int $id_form_input_child
 * @property string $value_string
 * @property float $value_float
 * @property int $value_integer
 * @property string $value_date_time
 * @property boolean $value_boolean
 * @property string $lang
 * @property string $created_at
 * @property string $updated_at
 * @property FormInputChild $formInputChild
 * @property FormInput $formInput
 */
class FormInputValue extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'form_input_value';

    /**
     * @var array
     */
    protected $fillable = ['id_form_input', 'id_form_input_child', 'value_string', 'value_float', 'value_integer', 'value_date_time', 'value_boolean', 'lang', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formInputChild()
    {
        return $this->belongsTo('App\Models\FormInputChild', 'id_form_input_child');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formInput()
    {
        return $this->belongsTo('App\Models\FormInput', 'id_form_input');
    }
}
