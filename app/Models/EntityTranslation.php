<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntityTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'content'];
}
